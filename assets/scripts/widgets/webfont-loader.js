const WebFont = require('webfontloader');
import { FONTS } from './variables.js';

class WebfontLoader {
    constructor(){
        WebFont.load(FONTS);
    }
}

module.exports = WebfontLoader;
