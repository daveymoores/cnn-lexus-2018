import { ARTICLE_DATA, ACTIONS } from './variables.js';

class Store {
    constructor(){
        this.prevState = {};
    	this.state = {};

        this.state = this.reduce(this.state, {});
    }

    getState(){
        return this.state;
    }

    getPrevState() {
    	return this.prevState;
    }

    dispatch(action) {
    	this.prevState = this.state;
    	this.state = this.reduce(this.state, action);

    	return action;
    }

    reduce(state, action) {
    	return {
    		article: this.updateArticle(state.article),
            action: this.updateAction(state.action)
    	};
    }

    updateArticle(article) {
    	article = article || ARTICLE_DATA[0].name;
        return article;
    }

    updateAction(action) {
        action = action || ACTIONS['end'];
        return action;
    }
}

module.exports = Store;
