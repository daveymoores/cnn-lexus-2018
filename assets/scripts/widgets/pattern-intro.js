
var PIXI = require("../../../dist/scripts/pixi.min.js");
require("../widgets/pixi-lights.min.js");
const ScrollMagic = require("scrollmagic");
const AnimationLoop = require('pixi-animationloop');
import ScrollHandler from './scroll-handler.js';
require('default-passive-events');

class PatternIntro {
    constructor() {
        this.setVars();
        this.init();
    }

    setVars(){
        this.css = {
            "selectors" : {
                "parent"            : "lex-scroll-hook-js",
                "canvasParent"      : "lex-canvas-wrap-js",
                "articleSelector"   : "[data-art]"
            }
        }

        this.positionVars = {
            i : 0,
            mouseX : 0,
            mouseY : 0,
            xPos : 0,
            yPos : 0,
            dX : 0,
            dY : 0
        }

        this.textures = [
            {
                "original" : "dist/images/article-bg/craftsmanship/bg.jpg",
                "normal" : "dist/images/article-bg/craftsmanship/bg-nm.jpg"
            },
            {
                "original" : "dist/images/article-bg/technology/bg.jpg",
                "normal" : "dist/images/article-bg/technology/bg-nm.jpg"
            },
            {
                "original" : "dist/images/article-bg/design/bg.jpg",
                "normal" : "dist/images/article-bg/design/bg-nm.jpg"
            },
            {
                "original" : "dist/images/article-bg/performance/bg.jpg",
                "normal" : "dist/images/article-bg/performance/bg-nm.jpg"
            }
        ]
    }

    init(){
        this.node = document.getElementById(this.css.selectors.parent);
        this.textureBgWrap = document.getElementById(this.css.selectors.canvasParent);
        this.fx_articles = this.node.querySelectorAll(this.css.selectors.articleSelector);
        this.textureDiv = this.textureBgWrap.querySelector('div');
        this.body = document.body;
        this.spinner = document.getElementById('lex-loader');

        this.renderer = new PIXI.lights.WebGLDeferredRenderer(window.innerWidth, window.innerHeight);
        new PIXI.AnimationLoop(this.renderer);
        this.loader = new PIXI.loaders.Loader();
        this.stage = new PIXI.Container();
        this.textureDiv.appendChild(this.renderer.view);

        this.preloadImages([this.textures[0].original, this.textures[0].normal, this.textures[1].original, this.textures[1].normal, this.textures[2].original, this.textures[2].normal, this.textures[3].original, this.textures[3].normal]);

        window.addEventListener("resize", this.resize.bind(this));
        this.resize();

        this.onScrollTrigger();
    }

    resize() {
        this.renderer.view.style.width = window.innerWidth;
        this.renderer.view.style.height = window.innerHeight;
    }

    preloadImages(array) {
        if (!this.preloadImages.list) {
            this.preloadImages.list = [];
        }
        var list = this.preloadImages.list;
        for (var i = 0; i < array.length; i++) {
            var img = new Image();
            img.onload = function() {
                var index = list.indexOf(this);
                if (index !== -1) {
                    list.splice(index, 1);
                }
            }
            list.push(img);
            img.src = array[i];
        }
    }

    onScrollTrigger(){

        const OPACITYLENGTH = 400;
        var HASITRUNFLAG = false;

        this.controller = new ScrollMagic.Controller({
            container: document.body,
            loglevel : 2
        });

        //set flag on scroll event and stop animation loop when out of screen
        [].forEach.call(this.fx_articles, (element, index, array)=>{

            var tweenIn = new TimelineMax().add([
                TweenMax.fromTo(this.textureDiv, 1, {opacity:0}, {opacity:1, ease: Linear.easeNone})
            ]);

            var tweenOut = new TimelineMax().add([
                TweenMax.fromTo(this.textureDiv, 1, {opacity:1}, {opacity:0, ease: Linear.easeNone})
            ]);

            this.opacityIn = new ScrollMagic.Scene({triggerElement: element, triggerHook: "onCenter", duration: OPACITYLENGTH, offset: 0})
                        .setTween(tweenIn)
                        .addTo(this.controller);

            this.opacityOut = new ScrollMagic.Scene({triggerElement: element, triggerHook: "onEnter", duration: OPACITYLENGTH, offset: -OPACITYLENGTH})
                                .setTween(tweenOut)
                                .addTo(this.controller);

            this.scene = new ScrollMagic.Scene({triggerElement: element, triggerHook: "onCenter", duration: element.getBoundingClientRect().height, offset: 0})
                            .on('enter', ()=>{

                                //if the loader hasn't been run before then run loader with lights
                                if(!this.body.classList.contains('background--loaded')) {

                                    //need to check url as it scroll through each article and throws and error
                                    var hash = window.location.hash.substring(1),
                                        value;

                                    switch (hash) {
                                        case 'craftsmanship':
                                            value = 0;
                                            break;
                                        case 'technology':
                                            value = 1;
                                            break;
                                        case 'design':
                                            value = 2;
                                            break;
                                        case 'performance':
                                            value = 3;
                                            break;
                                        default:
                                            value = 4;
                                    }

                                    //setting flag as it runs twice before pixi can load
                                    if(!HASITRUNFLAG){
                                        this.triggerLoader(this.textures[value].original, this.textures[value].normal, value);
                                        HASITRUNFLAG = true;
                                    }

                                } else { //else scrap textures from texturecache and load in new textures, then reset

                                    if(!(Object.keys(PIXI.TextureCache).length === 0 && PIXI.TextureCache.constructor === Object)){
                                        PIXI.TextureCache[this.loader.resources.bg_diffuse.url].destroy(true);
                                        PIXI.TextureCache[this.loader.resources.bg_normal.url].destroy(true);
                                    }

                                    this.loader.reset();
                                    this.loader.add('bg_diffuse', this.textures[index].original);
                                    this.loader.add('bg_normal', this.textures[index].normal);
                                    this.loader.load((loader, res) => {

                                        var bg = new PIXI.Sprite(res.bg_diffuse.texture);
                                        bg.normalTexture = res.bg_normal.texture;

                                        //then set heights and widths again
                                        var dims = this.imageRatioCalculation();

                                        bg.width = dims[0];
                                        bg.height = dims[1];
                                        bg.texture.width = dims[0];
                                		bg.texture.height = dims[1];

                                        this.stage.addChild(bg);

                                    });

                                }

                            })
                            .addTo(this.controller);

        });

    }

    imageRatioCalculation(){
        var minWidth = 1920,
            minHeight = 1079,
            ww,
            hh;

        if(window.innerWidth < 768){
            ww = (window.innerHeight/2)*3;
            hh = window.innerHeight;
        } else {
            ww = window.innerWidth;
            hh = (ww/3)*2 //rough ratio for image
        }

        return [ww, hh];
    }

    triggerLoader(img, imgNM, i){

        var ambientLightSetting;

        if(window.innerWidth < 768) {
            ambientLightSetting = 2;
        } else {
            ambientLightSetting = 0.6;
        }

        var cxt = this,
            bg,
            light = new PIXI.lights.PointLight(0xFED1E9, 2),
            stageLight = new PIXI.lights.AmbientLight(null, ambientLightSetting);

        this.loader
        .add('bg_diffuse', img)
        .add('bg_normal', imgNM)
        .load((loader, res) => {

            bg = new PIXI.Sprite(res.bg_diffuse.texture);
            bg.normalTexture = res.bg_normal.texture;
            bg.alpha = 1;
            light.position.set(200, 200);

            var dims = this.imageRatioCalculation();

            bg.width = dims[0];
            bg.height = dims[1];
            bg.texture.width = dims[0];
    		bg.texture.height = dims[1];

            light.height = 0.3;

            this.stage.addChild(bg);

            this.stage.addChild(stageLight);
            this.stage.addChild(light);

            window.addEventListener('load', (e)=>{
                var rect = this.textureBgWrap.getBoundingClientRect();

                light.position.x = window.innerWidth/2;
                light.position.y = window.innerHeight/2;
            });

            window.addEventListener('mousemove', (e)=>{
                var rect = this.textureBgWrap.getBoundingClientRect();
                this.setMousePosition(e, rect);
            });


            function animate(){
                cxt.positionVars.dX = cxt.positionVars.mouseX - cxt.positionVars.xPos;
                cxt.positionVars.dY = cxt.positionVars.mouseY - cxt.positionVars.yPos;

                cxt.positionVars.xPos += (cxt.positionVars.dX / 30);
                cxt.positionVars.yPos += (cxt.positionVars.dY / 30);

                light.position.x = cxt.positionVars.xPos;
                light.position.y = cxt.positionVars.yPos;

                requestAnimationFrame(animate);

                cxt.renderer.render(cxt.stage);
            }

            animate();
        });

        //loader set at the top of entrypoint.js
        //remove once pixi has loaded assets
        this.loader.once('complete', ()=>{
            this.body.classList.add('background--loaded');
            TweenMax.to(this.spinner.querySelector('svg'), 0.5, {opacity: 0, ease:Power1.easeInOut, onComplete:()=>{
                this.scrollHandler = new ScrollHandler();
                this.spinner.style.display = 'none';
            }});

        });
    }

    setMousePosition(e, rect){
        this.positionVars.mouseX = e.clientX - rect.left;
        this.positionVars.mouseY = e.clientY - rect.top;
    }
}

module.exports = PatternIntro;
