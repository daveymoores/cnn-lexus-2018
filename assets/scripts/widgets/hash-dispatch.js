class HashDispatch {
    constructor() {}

    dispatchandset(article){

        history.pushState(null, null, `#${article}`);

        this.createAndDispatch( window, 'adobetagevent', { action: 'hashchange' });

        // Safari, in Private Browsing Mode, looks like it supports localStorage but all calls to setItem
        // throw QuotaExceededError. We're going to detect this and just silently drop any calls to setItem
        // to avoid the entire page breaking, without having to do a check at each usage of Storage.
        if (typeof localStorage === 'object') {
            try {
                sessionStorage.setItem('prevUrl', window.location.hash);
            } catch (e) {
                Storage.prototype._setItem = Storage.prototype.setItem;
                Storage.prototype.setItem = function() {};
                alert('Your web browser does not support storing settings locally. In Safari, the most common cause of this is using "Private Browsing Mode". Some settings may not save or some features may not work properly for you.');
            }
        }

    }

    createAndDispatch( object, type, detail = {}){
        this.dispatch( object, this.create( type, detail ) );
    }

    create( type, detail = {} ){
        try {
           return new CustomEvent( type, { detail } );
        } catch( e ){
           var event = document.createEvent("CustomEvent");
           event.initCustomEvent( type, true, true, { detail } );
           return event;
        }
    }

    dispatch( object, event ){
        if( object.dispatchEvent ){
            object.dispatchEvent( event );
        } else if( object.fireEvent ){
            object.fireEvent("on" + event.type, event);
        }
    }
}

module.exports = HashDispatch;
