const plyr = require("plyr");
const scrollTo = require("ScrollTo");
const ScrollMagic = require("scrollmagic");
require("scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js");
const TimeLineMax = require("TimelineMax");
//var throttle = require('lodash.throttle');
//const HashNavigation = require('../widgets/hash-navigation.js');
//import Poll from './poll';
import ReactSocial from "./react-social.js";
import ReactNavigation from "./react-navigation.js";
import ReactArrow from "./react-arrow.js";
import { Promise } from "es6-promise-polyfill";
import getBreakpoint from "./beacons.js";
require("default-passive-events");

function DispatchAdobeTagEvent(detail) {
    if (window.dispatchEvent && CustomEvent) {
        window.dispatchEvent(new CustomEvent("adobetagevent", { detail }));
    }
}

function addCNNVideoTracking(plyrInstanceArray) {
    plyrInstanceArray.forEach(plyrInstance => {
        plyrInstance.on("ready", event => {
            var embed = plyrInstance.getEmbed();
            var title = "Awaiting title...";
            var id = "Awaiting url...";
            var duration = 0;

            embed.ready().then(function() {
                embed.on("ended", event =>
                    DispatchAdobeTagEvent({
                        source: id,
                        title: title,
                        action: "ended"
                    })
                );

                embed.on("play", event =>
                    DispatchAdobeTagEvent({
                        source: id,
                        title: title,
                        action: "play"
                    })
                );

                embed.on("pause", event =>
                    DispatchAdobeTagEvent({
                        source: id,
                        title: title,
                        action: "pause"
                    })
                );

                var lastMilestone = 0;

                embed.on("timeupdate", event =>
                    DispatchAdobeTagEvent({
                        source: id,
                        title: title,
                        action: "time",
                        milestone: event.percent - event.percent % 0.25 || 0,
                        isMilestone: (function() {
                            var m = event.percent - event.percent % 0.25 || 0;

                            if (
                                m - lastMilestone > 0.25 ||
                                m - lastMilestone < 0
                            ) {
                                lastMilestone = m;
                                return true;
                            } else {
                                return false;
                            }
                        })(),
                        value: event.percent,
                        duration
                    })
                );

                embed.getDuration().then(number => {
                    duration = number;
                });
                embed.getVideoTitle().then(string => {
                    title = string;
                    console.log("Added tracking to video " + title);
                });
                embed.getVideoId().then(string => {
                    id = string;
                });
            });
        });
    });
}

class ScrollHandler {
    constructor() {
        this.node = document.getElementById("lex-scroll-hook-js");
        this.setVars();
        this.init();
    }

    setVars() {
        this.css = {
            states: {
                contentLoaded: "content-loaded"
            }
        };
    }

    init() {
        //this.article = new HashNavigation();
        this.beacon = new getBreakpoint();

        //intro title flags
        this.freshLoadFlag = [true, true, true, true];
        this.transitionFlag = [true, true, true, true];
        this.scrollFlag = [true, true, true, true];
        this.HASITRUNFLAG = false;
        this.flag = true;

        var mobOFF_flag = false;
        if (this.beacon.breakpoint == "sm") {
            mobOFF_flag = true;
        }

        this.setupAnimations(mobOFF_flag).then(() => {
            console.log("Scroll effects hooked up");
        });

        window.addEventListener("resize", event => {
            if (window.innerWidth < 768) {
                mobOFF_flag = true;
            } else {
                mobOFF_flag = false;
            }

            this.resetController().then(() => {
                this.setupAnimations(mobOFF_flag, event).then(() => {
                    console.log("resize event");
                });
            });
        });
    }

    breakText(element, resolve) {
        return new Promise((resolve, reject) => {
            [].forEach.call(element, (e, i, a) => {
                var children = e.querySelectorAll("span"),
                    str,
                    strArray = [],
                    letterArray = [],
                    j,
                    i;

                [].forEach.call(children, (element, index, array) => {
                    if (element.textContent) {
                        (str = element.innerText),
                            (strArray = str.split(" ")),
                            (element.innerText = "");
                        element.classList.add("lex-split--parent");

                        for (i = 0; i < strArray.length; i++) {
                            letterArray = strArray[i].match(/\S/g);
                        }

                        for (j = 0; j < letterArray.length; j++) {
                            var span = document.createElement("span");
                            span.classList.add("lex-split--char");
                            span.innerText = letterArray[j];
                            span.style.opacity = 0;
                            element.appendChild(span);
                            resolve();
                        }
                    } else {
                        resolve();
                    }
                });
            });
        });
    }

    resetController() {
        return new Promise((resolve, reject) => {
            if (this.controller) {
                this.controller = this.controller.destroy(true);
                this.controller = null;
                this.scene = this.scene.destroy(true);
                this.scene = null;
                resolve();
            }

            if (this.mobOFF_controller) {
                this.mobOFF_controller = this.mobOFF_controller.destroy(true);
                this.mobOFF_controller = null;
            }
        });
    }

    //setup scroll handler for ScrollMagic and initialise scenes
    setupAnimations(flag, e = true) {
        return new Promise((resolve, reject) => {
            //two controllers so that effects not needed for mobile can be swicthed offset
            //all effects removed on mobile have prefix mobOFF_
            this.controller = new ScrollMagic.Controller({
                container: document.body,
                loglevel: 2
            });

            this.mobOFF_controller = new ScrollMagic.Controller({
                container: document.body,
                loglevel: 2
            });

            if (this.mobOFF_controller && flag == true) {
                this.mobOFF_controller = this.mobOFF_controller.destroy(true);
                this.mobOFF_controller = null;
            }

            var fx_articles = this.node.querySelectorAll("[data-art]");
            var fx_sections = this.node.querySelectorAll("[data-fx]");
            var body = document.body;

            [].forEach.call(fx_sections, (element, index, array) => {
                var effect = element.getAttribute("data-fx"),
                    elementHeight = element.getBoundingClientRect().height,
                    artId;

                switch (effect) {
                    case "title-sequence":
                        var wrapper = element.querySelector(
                                ".lex-header--pin-wrapper"
                            ),
                            progressLine = element.querySelector(
                                ".lex-header--ptag--progress-line"
                            ),
                            htag = wrapper.querySelectorAll(
                                ".lex-header--htag"
                            );

                        for (var i = 0; i < fx_articles.length; i++) {
                            var id = element.parentNode.getAttribute("id");
                            var articleId = fx_articles[i].getAttribute("id");

                            if (id == articleId) {
                                artId = i;
                            }
                        }

                        var tween = TweenMax.fromTo(
                            progressLine,
                            1,
                            { width: 130, ease: Linear.easeNone },
                            { width: 0, ease: Linear.easeNone }
                        );

                        //check for words to break and return promise for animation
                        this.breakText(htag).then(() => {
                            this.scene = new ScrollMagic.Scene({
                                triggerElement: element,
                                triggerHook: "onCenter",
                                duration: window.innerHeight,
                                offset: window.innerHeight / 2 - 30
                            })
                                .on("progress", event => {
                                    this.animationTypes(
                                        element,
                                        event,
                                        artId
                                    ).titleSequence.call();
                                })
                                .on("leave", event => {
                                    this.animationTypes(
                                        element,
                                        event,
                                        artId
                                    ).titleSequence.call();
                                })
                                .setTween(tween)
                                .setPin(wrapper)
                                .addTo(this.controller);
                        });

                        break;
                    case "sticky-clip":
                        var stickyWrapper = element.querySelector(
                                ".lex-parallax--sticky-vid-wrapper"
                            ),
                            stickyLength = element.getBoundingClientRect()
                                .height,
                            progressLine = element.querySelector(
                                ".lex-parallax--progress-line"
                            ),
                            videoPlayer = element.querySelectorAll(
                                "[data-type]"
                            );

                        var tween = TweenMax.fromTo(
                            progressLine,
                            1,
                            { width: 0, ease: Linear.easeNone },
                            { width: "100%", ease: Linear.easeNone }
                        );

                        if (e.type !== "resize") {
                            if (this.mobOFF_controller) {
                                //if a desktop video
                                if (this.video) {
                                    this.video[0].destroy();
                                    this.video = null;
                                }

                                if (!this.video) {
                                    this.video = plyr.setup(videoPlayer[0], {
                                        autoplay: false,
                                        clickToPlay: true, // so clicking doesn't pause it
                                        controls: ["mute", "play-large"], // no controls
                                        keyboardShorcuts: {
                                            focused: false,
                                            global: false
                                        }, // no keyboard interaction
                                        loop: false,
                                        muted: true
                                    });
                                    addCNNVideoTracking(this.video);

                                    this.video[0].on("ready", () => {
                                        videoPlayer[0].appendChild(
                                            progressLine
                                        );
                                    });
                                }

                                this.scene = new ScrollMagic.Scene({
                                    triggerElement: element,
                                    triggerHook: "onLeave",
                                    duration: stickyLength,
                                    offset: 0
                                })
                                    .on(
                                        "enter",
                                        this.animationTypes(stickyWrapper)
                                            .videoStart
                                    )
                                    .on(
                                        "leave",
                                        this.animationTypes(stickyWrapper)
                                            .videoStop
                                    )
                                    .setPin(stickyWrapper)
                                    .setTween(tween)
                                    .addTo(this.mobOFF_controller);
                            } else {
                                if (this.video) {
                                    this.video[0].destroy();
                                    this.video = null;
                                }

                                if (!this.video) {
                                    this.video = plyr.setup(videoPlayer[1], {
                                        autoplay: false,
                                        clickToPlay: true, // so clicking doesn't pause it
                                        controls: [
                                            "play-large",
                                            "play",
                                            "progress",
                                            "current-time",
                                            "mute",
                                            "volume",
                                            "fullscreen"
                                        ], // no controls
                                        keyboardShorcuts: {
                                            focused: false,
                                            global: false
                                        }, // no keyboard interaction
                                        loop: false,
                                        muted: true
                                    });
                                    addCNNVideoTracking(this.video);
                                }
                            }
                        }

                        break;
                    case "intro":
                        //check for words to break and return promise for animation

                        if (this.mobOFF_controller) {
                            var parallaxImg = element.querySelector("span"),
                                PARALLAXDISTANCE = 200,
                                tween = new TimelineMax().add([
                                    TweenMax.fromTo(
                                        parallaxImg,
                                        1,
                                        { y: PARALLAXDISTANCE },
                                        { y: -200, ease: Linear.easeNone }
                                    )
                                ]);

                            TweenMax.set(parallaxImg, { y: PARALLAXDISTANCE });

                            this.scene = new ScrollMagic.Scene({
                                triggerElement: element,
                                triggerHook: "onEnter",
                                duration: elementHeight * 2,
                                offset: 0
                            })
                                .setTween(tween)
                                .addTo(this.mobOFF_controller);
                        }

                        break;
                    case "content":
                        if (this.mobOFF_controller) {
                            var wrapper = element.querySelector(
                                    ".lex-parallax--img-wrapper"
                                ),
                                blockquote = wrapper.querySelector(
                                    ".lex-blockquote"
                                ),
                                img = wrapper.querySelector("span"),
                                tween = new TimelineMax().add([
                                    TweenMax.fromTo(
                                        blockquote,
                                        1,
                                        { x: -300 },
                                        { x: 50, ease: Linear.easeNone }
                                    ),
                                    TweenMax.fromTo(
                                        img,
                                        1,
                                        { x: -100 },
                                        { x: 0, ease: Linear.easeNone }
                                    )
                                ]);

                            TweenMax.set(blockquote, { x: -300 });
                            TweenMax.set(img, { x: -200 });

                            this.scene = new ScrollMagic.Scene({
                                triggerElement: element,
                                triggerHook: "onEnter",
                                duration: elementHeight * 2,
                                offset: 0
                            })
                                .setTween(tween)
                                .addTo(this.mobOFF_controller);
                        }

                        break;
                    case "sticky-heading":
                        if (this.mobOFF_controller) {
                            var stickyWrapper = element.querySelector(
                                    ".lex-parallax--sticky-vid-wrapper"
                                ),
                                stickyLength = element.getBoundingClientRect()
                                    .height;

                            this.scene = new ScrollMagic.Scene({
                                triggerElement: element,
                                triggerHook: "onLeave",
                                duration: stickyLength,
                                offset: 0
                            })
                                .setPin(stickyWrapper)
                                .addTo(this.mobOFF_controller);
                        }

                        break;
                    case "gallery":
                        var wrapper = element.querySelector(
                                ".lex-gallery--wrapper"
                            ),
                            galleryImg = wrapper.querySelectorAll(
                                ".lex-gallery--item"
                            ),
                            tween = new TimelineMax().add([
                                TweenMax.fromTo(
                                    galleryImg[0],
                                    1,
                                    { y: -300 },
                                    {
                                        y: 0,
                                        ease: Linear.easeNone,
                                        force3D: true
                                    }
                                ),
                                TweenMax.fromTo(
                                    galleryImg[1],
                                    1,
                                    { y: -250 },
                                    {
                                        y: 0,
                                        ease: Linear.easeNone,
                                        force3D: true
                                    }
                                ),
                                TweenMax.fromTo(
                                    galleryImg[2],
                                    1,
                                    { y: 100 },
                                    {
                                        y: 0,
                                        ease: Linear.easeNone,
                                        force3D: true
                                    }
                                ),
                                TweenMax.fromTo(
                                    galleryImg[3],
                                    1,
                                    { y: -400 },
                                    {
                                        y: 0,
                                        ease: Linear.easeNone,
                                        force3D: true
                                    }
                                ),
                                TweenMax.fromTo(
                                    galleryImg[4],
                                    1,
                                    { y: 400 },
                                    {
                                        y: 0,
                                        ease: Linear.easeNone,
                                        force3D: true
                                    }
                                ),
                                TweenMax.fromTo(
                                    galleryImg[5],
                                    1,
                                    { y: 500 },
                                    {
                                        y: 0,
                                        ease: Linear.easeNone,
                                        force3D: true
                                    }
                                ),
                                TweenMax.fromTo(
                                    galleryImg[6],
                                    1,
                                    { y: 300 },
                                    {
                                        y: 0,
                                        ease: Linear.easeNone,
                                        force3D: true
                                    }
                                )
                            ]);

                        this.scene_gallery = new ScrollMagic.Scene({
                            triggerElement: element,
                            triggerHook: "onEnter",
                            duration: window.innerHeight,
                            offset: 0
                        })
                            .on("enter", () => {
                                [].forEach.call(galleryImg, (e, i, a) => {
                                    e.style.willChange = "transform";
                                });
                            })
                            .on("leave", () => {
                                [].forEach.call(galleryImg, (e, i, a) => {
                                    e.style.willChange = "auto";
                                });
                            })
                            .setTween(tween)
                            .addTo(this.controller);

                        break;
                }

                if (index == array.length - 1) {
                    resolve();
                }
            });
        });
    }

    // different animations that are triggered off the data-effect attribute on sections
    animationTypes(element, e, index = null) {
        var tl = new TimeLineMax();

        return {
            titleSequence: () => {
                var headers = element.querySelectorAll(".lex-header--htag"),
                    articleTitleWrapper = element.querySelector(
                        ".lex-header--article-title-wrapper"
                    ),
                    siteTitleWrapper = element.querySelector(
                        ".lex-header--site-title-wrapper"
                    ),
                    progressLine = element.querySelector(
                        ".lex-header--ptag--progress-line"
                    ),
                    ptags = element.querySelectorAll(".lex-header--ptag"),
                    svg = element.querySelector(".lex-header--character"),
                    paths = svg.querySelectorAll(".target"),
                    body = document.body,
                    ctx = this;

                var length = [],
                    tweenArray = [],
                    pathLength = [],
                    timer = [],
                    distancePerPoint = 1,
                    drawFPS = 60;

                //Title width for correct placement of japanese character ---
                function getTitleWidth(title) {
                    var w = title.querySelector(".lex-split--parent"),
                        w = w.getBoundingClientRect().width;
                    return w;
                }

                //Drawing the svg ---
                function startDrawingPath(element, i, inReverse) {
                    length[i] = 0;

                    var obj = {
                        length: length[i],
                        pathLength: element.getTotalLength()
                    };
                    var time = inReverse ? 0.6 : 1.4; //change animation time dependant on direction

                    element.style.strokeDasharray = [0, 0].join(" ");
                    element.style.strokeDashoffset = -obj.pathLength;
                    element.style.stroke = "#f82456";

                    tweenArray[i] = TweenMax.to(obj, time, {
                        length: obj.pathLength,
                        onStart: () => {
                            svg.style.opacity = 1;
                        },
                        onUpdate: () => {
                            increaseLength(element, i, obj, inReverse);
                        },
                        onComplete: () => {
                            if (inReverse) {
                                svg.style.opacity = 0;
                            }
                        },
                        ease: Power1.easeInOut
                    });
                }

                //update function for svg
                function increaseLength(element, i, obj, inReverse) {
                    element.style.strokeDasharray = [
                        obj.pathLength,
                        obj.pathLength
                    ].join(" ");

                    if (inReverse) {
                        element.style.strokeDashoffset = -obj.length;
                    } else {
                        element.style.strokeDashoffset =
                            obj.length - obj.pathLength;
                    }
                }

                //animating the title characters in the header
                function animateHeading(element, ptag) {
                    return new Promise((resolve, reject) => {
                        var splitChars = element.querySelectorAll(
                            ".lex-split--char"
                        );

                        [].forEach.call(splitChars, (e, i, a) => {
                            TweenMax.set(e, { opacity: 0 }); //reset text characters
                        });
                        TweenMax.set(ptag, { x: 0 });

                        TweenMax.staggerTo(
                            splitChars,
                            0.6,
                            {
                                opacity: 1,
                                onStart: () => {
                                    TweenMax.to(ptag, 0.5, {
                                        opacity: 1,
                                        y: 20,
                                        ease: Power1.easeOut
                                    });
                                },
                                onComplete: () => {
                                    resolve();
                                },
                                ease: Power1.easeOut
                            },
                            0.03
                        );
                    });
                }

                function resetSiteHeading(element) {
                    headers[0].style.display = "block";
                    ptags[0].style.display = "block";

                    TweenMax.set(headers[0], { opacity: 1 });
                    TweenMax.set(headers[1], { opacity: 0 });

                    TweenMax.set(ptags[0], { opacity: 0, y: 0 });
                    TweenMax.set(ptags[1], { opacity: 0, y: 0, x: 0 });

                    TweenMax.set(svg, { opacity: 0 });
                    siteTitleWrapper.classList.add("visible");
                }

                function activatingTitleAtPageTop() {
                    //console.log('Activating title at top of page >>>>>');

                    function titleAnimationForHeading(delay) {
                        articleTitleWrapper.classList.remove("visible");
                        var splitChars = headers[1].querySelectorAll(
                            ".lex-split--char"
                        );
                        TweenMax.to(ptags[1], 0.5, {
                            opacity: 0,
                            y: 0,
                            x: 0,
                            ease: Power2.easeOut
                        });

                        [].forEach.call(splitChars, (element, index, array) => {
                            TweenMax.to(element, delay, {
                                delay: index * 0.03,
                                opacity: 0,
                                ease: Power2.easeOut,
                                onComplete: () => {
                                    TweenMax.to(siteTitleWrapper, 0.3, {
                                        opacity: 1,
                                        onStart: () => {
                                            siteTitleWrapper.classList.add(
                                                "visible"
                                            );
                                            siteTitleWrapper.style.display =
                                                "block";
                                        },
                                        onComplete: () => {
                                            TweenMax.set(element, {
                                                opacity: 0
                                            });
                                            resetSiteHeading(element);
                                            animateHeading(
                                                headers[0],
                                                ptags[0]
                                            ).then(() => {
                                                //ctx.flag = true;
                                            });
                                        }
                                    });
                                }
                            });
                        });
                    }

                    //do a check to make sure the other title is visible before animating it out
                    if (articleTitleWrapper.classList.contains("visible")) {
                        for (var i = 0; i < paths.length; i++) {
                            startDrawingPath(paths[i], i, true);
                        }
                        titleAnimationForHeading(0.3);
                    } else {
                        titleAnimationForHeading(0);
                    }
                }

                function showHeaderOnFirstLoad() {
                    //console.log('Showing header on first load >>>>>');

                    //when page loads create class that informs page where the header has first been activated
                    body.classList.add("heading--" + index);

                    resetSiteHeading(element);
                    animateHeading(headers[0], ptags[0]).then(() => {});
                }

                function animateArticleTitleIn(xtra) {
                    //console.log('Transition title in >>>>>');
                    articleTitleWrapper.style.display = "block";
                    articleTitleWrapper.classList.add("visible");

                    siteTitleWrapper.style.display = "none";
                    siteTitleWrapper.classList.remove("visible");

                    TweenMax.set(headers[1], { opacity: 1 });
                    animateHeading(headers[1], ptags[1]);

                    svg.style.marginLeft = `-${getTitleWidth(headers[1]) / 2 +
                        xtra}px`;
                    for (var i = 0; i < paths.length; i++) {
                        startDrawingPath(paths[i], i, false);
                    }

                    ctx.HASITRUNFLAG = true;
                }

                function animateArticleTitleAfterSiteTitle(xtra) {
                    //console.log('Transition from title sequence to article title >>>>>');
                    var splitChars = headers[1].querySelectorAll(
                        ".lex-split--char"
                    );

                    TweenMax.to(siteTitleWrapper, 0.6, {
                        opacity: 0,
                        onStart: () => {
                            siteTitleWrapper.classList.remove("visible");
                        },
                        onComplete: () => {
                            articleTitleWrapper.style.display = "block";
                            articleTitleWrapper.classList.add("visible");
                            headers[0].style.display = "none";
                            ptags[0].style.display = "none";
                            TweenMax.set(headers[1], { opacity: 1 });
                            animateHeading(headers[1], ptags[1]);

                            svg.style.marginLeft = `-${getTitleWidth(
                                headers[1]
                            ) /
                                2 +
                                xtra}px`;
                            for (var i = 0; i < paths.length; i++) {
                                startDrawingPath(paths[i], i, false);
                            }

                            TweenMax.staggerTo(
                                splitChars,
                                0.6,
                                {
                                    opacity: 1,
                                    onStart: () => {
                                        TweenMax.to(ptags[1], 0.5, {
                                            opacity: 1,
                                            y: 20,
                                            ease: Power1.easeOut
                                        });
                                    },
                                    ease: Power1.easeOut
                                },
                                0.03
                            );

                            //then reset for normal article headers
                            ctx.HASITRUNFLAG = true;
                        }
                    });
                }

                function animateTitleOut() {
                    //console.log('Title fading out >>>>>');
                    //check to see if the site title is visible on the page. if it is remove it.
                    var header = document.querySelectorAll(
                        ".lex-header--site-title-wrapper"
                    )[0];
                    if (header.classList.contains("visible")) {
                        var htag = header.querySelector(".lex-header--htag");
                        var ptag = header.querySelector(".lex-header--ptag");

                        TweenMax.to(header, 0.6, {
                            opacity: 0,
                            onStart: () => {
                                siteTitleWrapper.classList.remove("visible");
                            },
                            onComplete: () => {
                                htag.style.display = "none";
                                ptag.style.display = "none";
                            }
                        });

                        ctx.HASITRUNFLAG = true;
                    }

                    var splitChars = headers[1].querySelectorAll(
                        ".lex-split--char"
                    );

                    tl.set(ptags[1], { opacity: 1 }).staggerTo(
                        splitChars,
                        0.6,
                        {
                            opacity: 0,
                            onStart: () => {
                                TweenMax.to(ptags[1], 0.5, {
                                    opacity: 0,
                                    y: 0,
                                    x: 0,
                                    ease: Power2.easeOut
                                });
                                for (var i = 0; i < paths.length; i++) {
                                    startDrawingPath(paths[i], i, true);
                                }
                            },
                            onComplete: () => {
                                articleTitleWrapper.classList.remove("visible");
                            },
                            ease: Power1.easeOut
                        },
                        0.03
                    );
                }

                //LOGIC...or lack of it...
                //activating title at the top of the page
                if (
                    e.progress <= 0.15 &&
                    e.scrollDirection == "REVERSE" &&
                    element.parentNode.getAttribute("id") == "craftsmanship"
                ) {
                    if (this.flag) {
                        activatingTitleAtPageTop();
                        this.flag = false;
                    }

                    //reset flags
                    this.scrollFlag[index] = true;
                    this.transitionFlag[index] = false;
                    this.freshLoadFlag[index] = false;
                    this.HASITRUNFLAG = false;
                } else {
                    //if leaving the title allow it to reset
                    if (e.type == "leave") {
                        this.scrollFlag[index] = true;
                    }

                    //if progress gets to 0.15
                    //if there hasnt been a transition on the second title
                    //if its still on a fresh load
                    if (
                        e.progress >= 0.15 &&
                        this.transitionFlag[index] &&
                        this.freshLoadFlag[index] &&
                        !this.HASITRUNFLAG
                    ) {
                        this.scrollFlag[index] = true;
                        this.transitionFlag[index] = false;
                        this.freshLoadFlag[index] = false;
                    }

                    //on progress only allow it to run once
                    if (this.scrollFlag[index]) {
                        var xtra = 0;
                        if (window.innerWidth < 580) {
                            xtra = -10;
                        } else if (window.innerWidth < 768) {
                            xtra = 20;
                        } else {
                            xtra = 80;
                        }

                        if (e.state == "DURING") {
                            //--- first loaded page state
                            if (
                                this.freshLoadFlag[index] &&
                                !this.HASITRUNFLAG
                            ) {
                                //as this runs if you click to a next section, check that the page hasn't already loaded
                                //by checking body class heading--{index}
                                var bodyClass = body.getAttribute("class");
                                var classIndex = bodyClass.split(" ")[1];

                                if (
                                    classIndex == "heading--" + index ||
                                    classIndex === undefined
                                ) {
                                    showHeaderOnFirstLoad();
                                } else {
                                    animateArticleTitleIn(xtra);
                                    this.flag = true;
                                }
                            } else {
                                //--- when the title animates in
                                if (this.HASITRUNFLAG) {
                                    animateArticleTitleIn(xtra);
                                    this.flag = true;
                                }
                            }

                            // NOTE: transition to article title from site titleSequence
                            //if this flag is set then the title can transition to the article title
                            //runs only on a fresh load or at top or site
                            if (
                                !this.transitionFlag[index] &&
                                !this.freshLoadFlag[index] &&
                                !this.HASITRUNFLAG
                            ) {
                                var bodyClass = body.getAttribute("class");
                                var classIndex = bodyClass.split(" ")[1];

                                if (
                                    classIndex == "heading--" + index ||
                                    classIndex === undefined ||
                                    index == 0
                                ) {
                                    animateArticleTitleAfterSiteTitle(xtra);
                                    this.flag = true;
                                }
                            }
                        } else if (e.state == "AFTER" || e.state == "BEFORE") {
                            animateTitleOut();
                        }

                        this.scrollFlag[index] = false;
                    }

                    if (e.progress == 1 || e.type == "leave") {
                        this.scrollFlag[index] = true;
                    }
                }
            },
            videoStart: () => {
                if (this.video) {
                    this.video[0].volume = 0;
                    this.video[0].play();
                }
            },
            videoStop: () => {
                if (this.video) {
                    this.video[0].pause();
                }
            }
        };
    }
}

module.exports = ScrollHandler;
