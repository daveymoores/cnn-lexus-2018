var React = require('react');
var ReactDOM = require('react-dom');
const scrollTo = require('ScrollTo');
const ScrollMagic = require("scrollmagic");
require('scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js');


class ReactArrows extends React.Component {
    constructor(props){
        super(props);

        this.css = {
            "selectors" : {
                "injectionWrapper"  : "lex-injection-js",
                "header"            : ".lex-header",
                "articles"          : "[data-art]"
            }
        }

        this.injectionWrapper = document.getElementById(this.css.selectors.injectionWrapper);
        this.menuTrigger = this.injectionWrapper.querySelectorAll(this.css.selectors.header)[0];
        this.articleParents = this.injectionWrapper.querySelectorAll(this.css.selectors.articles);
        this.scenes = [];
        var tl = new TimelineMax();

        this.controller = new ScrollMagic.Controller({
            container: window,
            loglevel : 2
        });

        //transitioning menu on scroll position
        this.scene = new ScrollMagic.Scene({triggerElement: this.menuTrigger, triggerHook: "onCenter", duration: this.menuTrigger.getBoundingClientRect().height/3, offset: 0})
        .on('enter', (event)=>{
            this.scrollAnimation(event);
        })
        .on('leave', (event)=>{
            this.scrollAnimation(event);
        })
        .addTo(this.controller);

        //animation for down and up arrows
        [].forEach.call(this.articleParents, (e, i, a)=>{
            this.scenes = new ScrollMagic.Scene({
                        triggerElement: e,
                        triggerHook: "onLeave",
                        duration: window.innerHeight
                    })
                    .on('enter', (e)=>{
                        tl.clear();
                        tl.to(this.upChevron, 0.25, {opacity: 0, y: 0, ease:Power1.easeInOut})
                          .to(this.downChevron, 0.25, {opacity: 1, y: 0, ease:Power1.easeInOut});
                    })
                    .on('leave', (e)=>{
                        tl.clear();
                        tl.to(this.downChevron, 0.25, {opacity: 0, y: 0, ease:Power1.easeInOut})
                          .to(this.upChevron, 0.25, {opacity: 1, y: 0, ease:Power1.easeInOut});
                    })
                    .addTo(this.controller);
        });
    }

    componentDidMount(){
        this.downArrow = document.getElementById('lex-arrow--down');
        this.chevronParent = document.getElementById('lex-arrow--up');
        this.downChevron = this.chevronParent.querySelectorAll('svg')[0];
        this.upChevron = this.chevronParent.querySelectorAll('svg')[1];
    }

    scrollAnimation(event){
        if(event.state == 'AFTER' && event.scrollDirection == 'FORWARD') {

            TweenMax.to(this.downArrow, 0.3, {opacity: 0, y: -50, ease:Power1.easeInOut});
            TweenMax.to(this.chevronParent, 0.3, {opacity: 1, y: 0, ease:Power1.easeInOut});

        } else if(event.state == 'DURING' && event.scrollDirection == 'REVERSE') {

            TweenMax.to(this.downArrow, 0.3, {opacity: 1, y: 0, ease:Power1.easeInOut});
            TweenMax.to(this.chevronParent, 0.3, {opacity: 0, y: 10, ease:Power1.easeInOut});

        }
    }

    scrollUpArticle(e){
        e.preventDefault();

        var currentState = window.location.hash.substr(1);
        currentState = currentState.toString();
        TweenMax.to(window, 0.6, {scrollTo:`#${currentState}`});
    }

    render(){
        return(
            <div>
                <div className="lex-arrow--down-wrapper" id="lex-arrow--down">
                    <svg width="100%" viewBox="0 0 16.77 9.15">
                        <use xlinkHref="./dist/svg/svg-sprite.svg#down-chevron" />
                    </svg>
                </div>
                <div className="lex-arrow--up-wrapper" id="lex-arrow--up" onClick={ this.scrollUpArticle.bind(this) }>
                    <svg width="100%" viewBox="0 0 16.77 9.15">
                        <use xlinkHref="./dist/svg/svg-sprite.svg#down-chevron" />
                    </svg>
                    <svg width="100%" viewBox="0 0 16.77 10.84">
                        <use xlinkHref="./dist/svg/svg-sprite.svg#back-to-top" />
                    </svg>
                </div>
            </div>
        )
    }
}

ReactDOM.render(<ReactArrows />, document.getElementById('lex-arrow-js'));
