class HashNavigation {
    constructor(){
        this.parseUrl();
    }

    parseUrl(){
        var url = window.location.hash;
        url = url.split('=');
        url.shift();
        return url;
    }
}

module.exports = HashNavigation;
