const breakpoints = ["sm", "md", "lg"];

class getBreakpoint {
    constructor(){
        for (var i = 0; i < breakpoints.length; i++) {
          if (this.isBreakpoint(breakpoints[i])) {
            return {breakpoint: breakpoints[i]};
          }
        }
        return "base";
    }

    isBreakpoint(breakpoint){
        var beacon = document.getElementById("js-" + breakpoint + "-beacon");
        if (beacon) {
          return beacon.offsetWidth > 0;
        }
        return false;
    }
}

module.exports = getBreakpoint;
