import {Promise} from "es6-promise-polyfill";

class FetchArticleJSON {

    constructor() {}

    init(url, callback) {
        var request = new XMLHttpRequest();
        request.open('GET', url, true);

        request.onload = function() {
          if (request.status >= 200 && request.status < 400) {
            var data = JSON.parse(request.responseText);
            callback(null, data);
          } else {
            console.log('cant find the article data');
          }
        };

        request.onerror = function() {
            console.log('connection error');
        };

        request.send();
    }

}

module.exports = FetchArticleJSON;
