import {Promise} from "es6-promise-polyfill";
import "webcomponents-lite"
import Poll from './poll';

class LoadContent {
    constructor(content){}

    init(content){
        this.injectionContainer = document.getElementById('lex-injection-js');

        this.content = content;
        return new Promise((resolve, reject) => {
            //empty dom
            this.emptyCurrentContent().then(() => {
                //get content from import
                this.getContent(this.content).then(() => {
                    //check fonts have loaded before displaying loading animation
                    this.checkFontstatus().then(() => {
                        resolve();
                    });
                    
                }).catch(function(url){ // handle an image not loading
                    console.log('Error loading article');
                });
            });
        });
    }

    emptyCurrentContent(){
        var parent = this.injectionContainer;
        return new Promise((resolve, reject) => {
            // NOTE: remove timeout
            setTimeout(()=>{
                parent.innerHTML = "";
                resolve();
            }, 200);
        });
    }

    checkFontstatus(){
        //-- poll.js
        return Poll(function() {
            var html = document.documentElement;
            var classBool = html.classList.contains('wf-active');
            return classBool;
        }, 2000, 150).then(function() {
            console.log('fonts loaded');
            return true;
        });

    }

    getContent(url){
        return new Promise((resolve, reject) => {
            var link = document.createElement('link');
            link.setAttribute('rel', 'import');
            link.setAttribute('href', url);

            function cloneTemplate(templateElement) {
                try {
                    return document.importNode(templateElement.content, true);
                }
                catch (e) {
                    var wrapper = document.createElement('div'),
                        fragment = document.createDocumentFragment();
                    wrapper.innerHTML = templateElement.innerHTML;
                    while (wrapper.firstChild) {
                        var child = wrapper.removeChild(wrapper.firstChild);
                        fragment.appendChild(child);
                    }
                    return fragment;
                }
            }

            link.onload = (e) => {
                link = e.target;
                var template = link.import.querySelector('template');
                var clone = cloneTemplate(template);
                var wrapper = this.injectionContainer;
                wrapper.appendChild(clone);
                resolve();
            };
            link.onerror = function(){
                reject()
            }
            document.body.appendChild(link);
            link = null;
        });
    }
}

module.exports = LoadContent;
