export const TEXTURES = {
    'craftsmanship' : './dist/images/article-bg/texture-1.jpg',
    'technology' : './dist/images/article-bg/texture-2.jpg',
    'design' : './dist/images/article-bg/texture-3.jpg',
    'performance' : './dist/images/article-bg/texture-4.jpg'
}

export const ARTICLE_DATA = [
    {
        "name" : "craftsmanship",
        "displayName" : "Craftsmanship",
        "texture" : "./dist/images/article-bg/texture-1.jpg"
    },
    {
        "name" : "technology",
        "displayName" : "Technology",
        "texture" : "./dist/images/article-bg/texture-2.jpg"
    },
    {
        "name" : "design",
        "displayName" : "Design",
        "texture" : "./dist/images/article-bg/texture-3.jpg"
    },
    {
        "name" : "performance",
        "displayName" : "Performance",
        "texture" : "./dist/images/article-bg/texture-4.jpg"
    }
]

export const COLORS = {
    'pink' : '#FC80B5'
}

export const SOCIAL = [
    {
        "name"  : "Twitter",
        "url"   : "https://twitter.com/home?status=Pioneering%20Spirits.%20These%20skilful,%20dedicated%20individuals%20are%20committed%20to%20perfection,%20and%20the%20legacy%20of%20their%20craft",
        "img"   : "./dist/images/social/twitter.svg"
    },
    {
        "name"  : "Facebook",
        "url"   : "https://www.facebook.com/sharer/sharer.php?u=http%3A//advertisementfeature.cnn.com/2018/lexuspioneeringspirits/",
        "img"   : "./dist/images/social/facebook.svg"
    },
    {
        "name"  : "LinkedIn",
        "url"   : "https://www.linkedin.com/shareArticle?mini=true&url=https%3A//www.facebook.com/sharer/sharer.php?u=http%253A//advertisementfeature.cnn.com/2018/lexuspioneeringspirits/&title=CNN%20-%20Lexus%20Pioneering%20Spirits&summary=Pioneering%20Spirits.%20These%20skilful,%20dedicated%20individuals%20are%20committed%20to%20perfection,%20and%20the%20legacy%20of%20their%20craft.&source=",
        "img"   : "./dist/images/social/linkedin.svg"
    }
]


export const FONTS = {
    google: {
      families: ['Montserrat:400,500, 600,800,900']
    }
}

export const ACTIONS = {
    'end' : 'finished animating',
    'start' : 'about to start animating',
    'animating' : 'animating in progress'
}
