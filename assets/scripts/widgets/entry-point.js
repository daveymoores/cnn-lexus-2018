import Poll from './poll';
import {Promise} from "es6-promise-polyfill";
import HashDispatch from './hash-dispatch.js';
import { ACTIONS, TEXTURES, COLORS } from './variables.js';
const TimeLineMax = require("TimelineMax");
const LazyLoad = require("vanilla-lazyload");
const scrollTo = require('ScrollTo');
const ScrollMagic = require("scrollmagic");
require('scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js');
import PatternIntro from './pattern-intro.js';
require('default-passive-events');



class EntryPoint {
    constructor(node){
        this.node = node;
        this.setVars();
        this.init();
    }

    setVars(){
        this.css = {
            "selectors" : {
                "mask" : "lex-mask-js"
            }
        }
    }

    init(){
        var myLazyLoad = new LazyLoad({
            elements_selector: ".lazy"
        });
        this.PIXITexture = new PatternIntro();
        this.hashDispatch = new HashDispatch();
        this.mask =  document.getElementById(this.css.selectors.mask);
        this.parent = document.getElementById('lex-scroll-hook-js');
        this.articleParents = this.parent.querySelectorAll('[data-art]');
        this.navigation = document.getElementById('lex-navigation-js');
        this.navigationItems = this.navigation.querySelectorAll('.lex-navigation--item');
        this.setUrl();
        var cxt = this;

        var tl = new TimeLineMax();
        tl.to(this.mask, 1.5, {opacity: 0, ease:Power1.easeInOut});
    }

    setUrl(){
        this.controller = new ScrollMagic.Controller({
            container: document.body,
            loglevel : 2
        });

        var url = window.location.hash;
        if(url == ""){
            //if no hash set  url to first article
            history.pushState(null, null, `#craftsmanship`);
            url = "craftsmanship";
            var loc = document.getElementById("craftsmanship");
            loc.classList.add('first-time-navigation');
        } else {
            var currentState = window.location.hash.substr(1);
            var loc = document.getElementById(currentState);
            loc.classList.add('first-time-navigation');
        }

        [].forEach.call(this.articleParents, (e, i, a)=>{

            // NOTE: would be good to fix this
            //have to do some hacky stuff here because the video
            //mess's with height calculation of the article
            var _extra = 0;
            var _id = e.getAttribute('id');
            if(_id == 'craftsmanship') {
                var videoSection = e.querySelector('.lex-parallax--sticky-vid-parent');
                _extra = videoSection.getBoundingClientRect().height;
            }

            this.scene = new ScrollMagic.Scene({
                    triggerElement: e,
                    triggerHook: "onEnter",
                    duration: (e.getBoundingClientRect().height + _extra)
                })
                .on('enter', ()=>{
                        var id = e.getAttribute('id');
                        history.pushState(null, null, `#${id}`);
                        this.navigationItems[i].classList.add('active');
                        TweenMax.to(this.navigationItems[i], 0.3, {opacity:1, ease: Linear.easeNone});
                        this.hashDispatch.dispatchandset(id);
                    })
                    .on('leave', ()=>{
                        var id = e.getAttribute('id');
                        this.navigationItems[i].classList.remove('active');
                        TweenMax.to(this.navigationItems[i], 0.3, {opacity:0.6, ease: Linear.easeNone});
                    })
                    .addTo(this.controller);
        });
    }
}


module.exports = EntryPoint;
