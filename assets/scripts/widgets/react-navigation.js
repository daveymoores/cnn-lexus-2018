var React = require('react');
var ReactDOM = require('react-dom');
const scrollTo = require('ScrollTo');
const ScrollMagic = require("scrollmagic");
import HashNavigation from './hash-navigation.js';
const HashDispatch = require('../widgets/hash-dispatch.js');
import { ARTICLE_DATA, TEXTURES } from './variables.js';
require('scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js');
const TimeLineMax = require("TimelineMax");
import getBreakpoint from './beacons.js';
require('default-passive-events');


class ReactNavigation extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            activeIndex: null
        }

        this.css = {
            "selectors" : {
                "wrapper"           : "lex-navigation-js",
                "wrapperParent"     : "lex-scroll-hook-js",
                "injectionWrapper"  : "lex-injection-js",
                "header"            : ".lex-header",
                "mobBtn"            : ".lex-navigation--mobile-cta"
            },
            "states" : {
                "articleInactive"   : "no-article-selected",
                "articleActive"     : "article-selected",
                "active"            : "active",
                "visible"           : "visible"
            }
        }

        this.wrapper = document.getElementById(this.css.selectors.wrapper);
        this.wrapperParent = document.getElementById(this.css.selectors.wrapperParent);
        this.injectionWrapper = document.getElementById(this.css.selectors.injectionWrapper);
        this.menuTrigger = this.injectionWrapper.querySelectorAll(this.css.selectors.header)[0];
        this.title = document.getElementById('lex-title');
        this.body = document.body;

        this.data = ARTICLE_DATA;
        this.article = new HashNavigation();
        this.hashDispatch = new HashDispatch();
        this.beacon = new getBreakpoint();

        var url = this.article.parseUrl()
        switch (url.toString()) {
            case this.data[0].name:
                this.setState({ activeIndex: 0 });
                break;
            case this.data[1].name:
                this.setState({ activeIndex: 1 });
                break;
            case this.data[2].name:
                this.setState({ activeIndex: 2 });
                break;
            case this.data[3].name:
                this.setState({ activeIndex: 3 });
                break;
            default:
                this.setState({ activeIndex: 0 });
        }

        this.setLocation = this.setLocation.bind(this);
        this.revealMobMenu = this.revealMobMenu.bind(this);
        this.flag = false;
        this.mobVisibilityFlag = false;
        this.animateInProgressFlag = false;
        this.setResponsiveDims();

        window.addEventListener('resize', (e)=>{
            this.setResponsiveDims();
        });
    }

    componentDidMount(){
        console.log('component mounted');
        this.monitorScroll();
        this.attachScrollHandlers();
    }

    setResponsiveDims(){
        //change xposition dependant on mobile styling
        var ww = window.innerWidth;
        if(ww < 768) {
            this.responsiveDims = {
                'xPos' : 0,
                'wrapperPos' : "-100%"
            }
        } else {
            this.responsiveDims = {
                'xPos' : "-50%",
                'wrapperPos' : -300
            }
        }
    }

    setLocation(index, location, e){
        e.preventDefault();
        var ww = window.innerWidth;
        const currentState = this.state.active;
        //set active state on list item
        this.setState({ activeIndex: index });
        //scroll window
        if(location == 'craftsmanship') {
            TweenMax.to(window, 0, {scrollTo:200});
        } else {
            TweenMax.to(window, 0, {scrollTo:`#${location}`});
        }

        if(!this.wrapperParent.classList.contains(this.css.states.articleInactive)){
            if(ww < 768 && (window.pageYOffset >= 200)) {
                this.revealMobMenu(e);
            } else {
                TweenMax.to(this.wrapper, 0.6, {backgroundColor:"transparent", delay: 0.4, ease:Power1.easeInOut});
                this.toggleMenuStates();
            }
        }
    }

    monitorScroll(){
        //this sets the gradients on each side of the mobile nav
        var scrollWrapper = this.wrapper.querySelector('.lex-navigation--scroll-container');
        var navList = this.wrapper.querySelector('.lex-navigation');

        var navListWidth = navList.getBoundingClientRect().width;

        this.controllerHorizontal = new ScrollMagic.Controller({
            container: scrollWrapper,
            loglevel : 2,
            vertical: false
        });

        this.sceneHorizontal = new ScrollMagic.Scene({
                triggerElement: navList,
                triggerHook: "onEnter",
                duration: navListWidth,
                offset: 0})
                .on('update', (event)=>{
                    if(event.scrollPos == 0) {
                        scrollWrapper.classList.add('scrolled-to-beginning');
                        scrollWrapper.classList.remove('scrolled-to-end');
                    } else if(event.scrollPos == event.endPos) {
                        scrollWrapper.classList.remove('scrolled-to-beginning');
                        scrollWrapper.classList.add('scrolled-to-end');
                    } else {
                        scrollWrapper.classList.add('scrolled-to-beginning');
                        scrollWrapper.classList.add('scrolled-to-end');
                    }
                })
                .addTo(this.controllerHorizontal);
    }

    attachScrollHandlers(){

        this.controller = new ScrollMagic.Controller({
            container: window,
            loglevel : 2
        });

        //transitioning menu on scroll position
        this.scene = new ScrollMagic.Scene({triggerElement: this.menuTrigger, triggerHook: "onCenter", duration: this.menuTrigger.getBoundingClientRect().height/3, offset: 0})
        .on('enter', ()=>{
            if(this.flag){ //flag to prevent the menu transitioning on load
                this.transitionMenuToNavigationState();
            }
        })
        .on('leave', ()=>{
            this.flag = true; //now allow transition
            this.transitionMenuToScrollState();
        })
        .addTo(this.controller);
    }

    transitionMenuToScrollState(){
        //this the layout for scrolling down the page
        var items = this.wrapper.querySelectorAll('li');
        var share = document.getElementById('lex-share-icon');
        var shareAnchor = share.querySelector('a');

        var tl = new TimeLineMax({
            onComplete: ()=>{
                this.wrapperParent.classList.remove(this.css.states.articleInactive);
            }
        });

        tl.to(this.wrapper, 0.6, {y:100, x:this.responsiveDims.xPos, ease:Power1.easeInOut})
          .add(()=>{
              this.wrapperParent.classList.remove(this.css.states.articleInactive);
              TweenMax.set(this.wrapper, {y:0, x:this.responsiveDims.wrapperPos});
              TweenMax.to(shareAnchor, 0.3, {autoAlpha: 0, ease:Power1.easeInOut, onComplete:()=>{
                  TweenMax.set(shareAnchor, {right: 20, bottom: 10, top: 'auto', left: 'auto'});
                  TweenMax.to(shareAnchor, 0.3, {autoAlpha: 1, ease:Power1.easeInOut});
              }});
              [].forEach.call(items, (e, i, a)=>{
                  TweenMax.set(e, {y:-5, x:0, autoAlpha:0});
              });
              TweenMax.to(this.title, 0.25, {y:0, autoAlpha: 1, ease:Power1.easeInOut});
          })
          .to(this.wrapper, 0.3, {x:"0%", ease:Power1.easeInOut})
          .add(()=>{
              [].forEach.call(items, (e, i, a)=>{
                  setTimeout(()=>{
                      var op = 0.7;
                      if(e.classList.contains(this.css.states.active)) {
                          op = 1;
                      }

                      TweenMax.to(e, 0.3, {y:0, autoAlpha:op, ease:Power1.easeInOut});
                  }, 30*i);
              });
          });
    }

    transitionMenuToNavigationState(){
        //this is the layout for being at the top of the page
        var tl = new TimeLineMax();
        var ul = this.wrapper.querySelector('ul');
        var items = this.wrapper.querySelectorAll('li');
        var share = document.getElementById('lex-share-icon');
        var shareAnchor = share.querySelector('a');

        tl.add(()=>{
              [].forEach.call(items, (e, i, a)=>{
                  setTimeout(()=>{
                      TweenMax.to(e, 0.3, {y:-5, autoAlpha:0, ease:Power1.easeInOut, onComplete:()=>{
                          [].forEach.call(items, (e, i, a)=>{
                              e.setAttribute('style', '');
                          });
                      }});
                  }, 30*i);
              });

              TweenMax.to(shareAnchor, 0.3, {autoAlpha: 0, ease:Power1.easeInOut, onComplete:()=>{
                  shareAnchor.setAttribute('style', '');
                  TweenMax.to(shareAnchor, 0.3, {autoAlpha: 1, ease:Power1.easeInOut});
              }});

              TweenMax.to(this.title, 0.25, {y:-10, autoAlpha: 0, ease:Power1.easeInOut});
          })
          .to(this.wrapper, 0.3, {x:this.responsiveDims.wrapperPos, ease:Power1.easeInOut})
          .add(()=>{
              ul.setAttribute('style', '');
              this.wrapperParent.classList.add(this.css.states.articleInactive);
              TweenMax.set(this.wrapper, {y:100, x:this.responsiveDims.xPos});
              [].forEach.call(items, (e, i, a)=>{
                  TweenMax.set(e, {y:0, x:0, autoAlpha:1});
              });
          })
          .to(this.wrapper, 0.3, {y:0, x:this.responsiveDims.xPos, ease:Power1.easeInOut, onComplete:()=>{
              this.wrapper.setAttribute('style', '');
          }});
    }

    toggleMenuStates(){
        var ww = window.innerWidth;
        if(ww < 768) {
            this.mobBtn = this.wrapper.querySelector(this.css.selectors.mobBtn);
            if(this.mobVisibilityFlag) {
                this.mobVisibilityFlag = false;
                this.mobBtn.innerText = "Menu";
                this.body.classList.remove('lex-navigation--menu-open');
            } else {
                this.mobVisibilityFlag = true;
                this.mobBtn.innerText = "Close";
                this.body.classList.add('lex-navigation--menu-open');
            }
        }
    }

    revealMobMenu(e){
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();

        var ul = this.wrapper.querySelector('ul');
        var items = this.wrapper.querySelectorAll('li');
        var active = ul.querySelector('.' + this.css.states.active);
        var tl = new TimeLineMax();

        //kill click event if the social menu is visible
        if(this.body.classList.contains('lex-social--menu-open')) {
            return false;
        }

        //kill click event if animating
        if(this.animateInProgressFlag) {
            return false;
        }

        if(this.mobVisibilityFlag) {
            //if mobile menu is visible
            this.animateInProgressFlag = true;

            tl.staggerTo(items, 0.6, {autoAlpha:0, y:-10, x:0, ease:Power2.easeOut}, 0.05)
                .to(ul, 0.3, {autoAlpha:0, ease:Linear.easeNone}, '-=0.3')
                .to(this.wrapper, 0.5, {backgroundColor:"transparent", ease:Power1.easeInOut, onComplete:()=>{
                    this.animateInProgressFlag = false;

                    this.toggleMenuStates();
                    setTimeout(()=>{
                        ul.setAttribute('style', '');
                        [].forEach.call(items, (e, i, a)=>{
                            var op = 0.7;
                            if(e.classList.contains(this.css.states.active)) {
                                op = 1;
                            }
                            TweenMax.set(e, {autoAlpha:op, y: 0});
                        });

                        TweenMax.set(this.wrapper, {height: 100});
                    }, 200);

                }}, '-=0.6');

        } else {
            //if mobile menu isnt visible
            this.animateInProgressFlag = true;
            ul.style.display = "block";

            [].forEach.call(items, (e, i, a)=>{
                TweenMax.set(e, {y: -10, autoAlpha: 0});
            });

            tl.to(this.wrapper, 0.3, {backgroundColor:"#000000", ease:Power1.easeInOut, onStart:()=>{
                    TweenMax.set(this.wrapper, {height: '100%'});
                }})
                .to(ul, 0.3, {autoAlpha:1, ease:Linear.easeNone}, '-=0.3')
                .add(()=>{
                    [].forEach.call(items, (e, i, a)=>{
                        var op = 0.7;
                        if(e.classList.contains(this.css.states.active)) {
                            op = 1;
                        }
                        TweenMax.to(e, 0.6, {autoAlpha:op, y: 0, delay:0.05*i, ease:Power2.easeInOut});

                        if(i==a.length-1){
                            this.animateInProgressFlag = false;
                            this.toggleMenuStates();
                        }
                    });
                });
        }
    }

    render(){
        return(
            <div class="lex-navigation--scroll-container">
                <NavMobLink onClick={ this.revealMobMenu } />
                <ul className="lex-navigation">
                    <NavItem name={ this.data[0].displayName } location={ this.data[0].name } index={0} isActive={ this.state.activeIndex===0 } onClick={ this.setLocation } />
                    <NavItem name={ this.data[1].displayName } location={ this.data[1].name } index={1} isActive={ this.state.activeIndex===1 } onClick={ this.setLocation } />
                    <NavItem name={ this.data[2].displayName } location={ this.data[2].name } index={2} isActive={ this.state.activeIndex===2 } onClick={ this.setLocation } />
                    <NavItem name={ this.data[3].displayName } location={ this.data[3].name } index={3} isActive={ this.state.activeIndex===3 } onClick={ this.setLocation } />
                </ul>
                <span className="lex-navigation--mob-dir-prompt">
                    <svg width="100%" viewBox="0 0 600 400">
                        <use xlinkHref="./dist/svg/svg-sprite.svg#right-chevron" />
                    </svg>
                </span>
            </div>
        )
    }

}

class NavMobLink extends React.Component {
    constructor(props) {
        super(props);
    }

    revealMobMenu(e){
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
        this.props.onClick(e);
    }

    render(){
        return (
            <a href="javascript:;" className="lex-navigation--mobile-cta" onClick={ (e) => {this.revealMobMenu(e)}}>Menu</a>
        )
    }
}

class NavItem extends React.Component {
    constructor(props) {
        super(props);
    }

    setLocation(e){
        this.props.onClick(this.props.index, this.props.location, e);
    }

    render(){
        return(
            <li className={this.props.isActive ? 'active lex-navigation--item': 'lex-navigation--item'} onClick={ this.setLocation.bind(this) }>
                <a href="javascript:;" data-location={ this.props.location }>{ this.props.name }</a>
            </li>
        )
    }
}


ReactDOM.render(<ReactNavigation />, document.getElementById('lex-navigation-js'));
