var React = require('react');
var ReactDOM = require('react-dom');
import { SOCIAL } from './variables.js';

class ReactSocial extends React.Component {
    constructor(props) {
        super(props);
        this.social = SOCIAL;

        this.state = {
            menuOpen: false
        }

        this.animateInProgressFlag = false;

        this.toggleMobileShareDialogue = this.toggleMobileShareDialogue.bind(this);
        this.closeMobileShareDialogue = this.closeMobileShareDialogue.bind(this);

        this.socialParent = document.getElementById('lex-social-js');
        this.body   = document.body;
        this.canvas = document.createElement("canvas");
        this.ctx    = this.canvas.getContext("2d");
        this.angle  = Math.PI * 2;
        this.width;
        this.height;
    }

    removeCanvas() {
      this.canvas.remove();
    }

    drawRipple(ripple) {
      this.ctx.clearRect(0, 0, this.width, this.height);
      this.ctx.beginPath();
      this.ctx.arc(ripple.x, ripple.y, ripple.radius, 0, this.angle, false);
      this.ctx.fillStyle = "rgba(0,0,0," + ripple.alpha + ")";
      this.ctx.fill();
    }

    maxDistance(x, y) {
      var point = { x: x, y: y };
      var da = this.distanceSq(point, { x: 0, y: 0 });
      var db = this.distanceSq(point, { x: this.width, y: 0 });
      var dc = this.distanceSq(point, { x: this.width, y: this.height });
      var dd = this.distanceSq(point, { x: 0, y: this.height });
      return Math.sqrt(Math.max(da, db, dc, dd));
    }

    distanceSq(p1, p2) {
      return Math.pow(p1.x - p2.x, 2) + Math.pow(p1.y - p2.y, 2);
    }

    createRipple(event) {
        var socialItems = this.socialParent.querySelectorAll('.lex-social__item');
        var socialClose = this.socialParent.querySelector('.lex-social--close');

        if (event.target === this.canvas) return;

        this.canvas.classList.add('lex-social__menu-bg');
        this.body.appendChild(this.canvas);

        this.width  = this.canvas.width  = this.body.scrollWidth;
        this.height = this.canvas.height = window.innerHeight;

        var x = event.pageX;
        var y = event.pageY;
        var radius = this.maxDistance(x, y);

        var ripple = {
            alpha: 1,
            radius: 0,
            x: x,
            y: y
        };

        var tl = new TimelineMax({ onStart:()=>{ this.animateInProgressFlag = true }, onUpdate: this.drawRipple.bind(this, ripple), onComplete: ()=>{
            [].forEach.call(socialItems, (e, i, a)=>{
                //make icons animate except the mobBtn
                if(i!==a.length-1){
                    TweenMax.to(e, 0.6, {autoAlpha:1, delay:0.07*i, ease:Power1.easeInOut, onStart: ()=>{
                        e.style.display = "block";
                    }});
                }
                //reset animation flag
                if(i==a.length-1){
                    this.animateInProgressFlag = false;
                }
            });
        }})
        .to(ripple, 0.7, { alpha: 1, radius: radius })
        .to(socialClose, 0.5, {autoAlpha: 1, ease:Power1.easeInOut}, "-=0.5");
    }

    closeMobileShareDialogue(){
        var socialItems = this.socialParent.querySelectorAll('.lex-social__item');
        var socialClose = this.socialParent.querySelector('.lex-social--close');

        var x = event.pageX;
        var y = event.pageY;
        var radius = this.maxDistance(x, y);

        var ripple = {
            alpha: 1,
            radius: radius,
            x: x,
            y: y
        };

        var tl = new TimelineMax({ onStart:()=>{ this.animateInProgressFlag = true }, onUpdate: this.drawRipple.bind(this, ripple) })
        .add(()=>{
            [].forEach.call(socialItems, (e, i, a)=>{
                //make icons animate except the mobBtn
                if(i!==a.length-1){
                    TweenMax.to(e, 0.3, {autoAlpha:0, delay:0.07*i, ease:Power1.easeInOut, onComplete:()=>{
                        e.style.display = "none";
                        this.removeCanvas();
                    }});
                }
            });
            TweenMax.to(socialClose, 0.5, {autoAlpha: 0, ease:Power1.easeInOut})
        })
        .to(ripple, 0.7, { alpha: 1, radius: 0, delay:0.3, oncomplete:()=>{
            //reset animation flag
            this.animateInProgressFlag = false;
        }});
    }

    toggleMobileShareDialogue(menuOpen, e){
        //kill click event if the navigation menu is visible
        if(this.body.classList.contains('lex-navigation--menu-open')) {
            return false;
        }
        //kill click event if animating
        if(this.animateInProgressFlag) {
            return false;
        }

        if(this.state.menuOpen) {
            this.setState({ menuOpen: false });
            this.closeMobileShareDialogue();
            this.body.classList.remove('lex-social--menu-open');
        } else {
            this.setState({ menuOpen: true });
            this.createRipple(e);
            this.body.classList.add('lex-social--menu-open');
        }
    }

    render(){
        return (
            <div>
                <ul className="lex-social">
                    <li className="lex-social__item lex-social__item--twitter">
                        <a href={ this.social[0].url } target="_blank">
                            { this.social[0].name }
                            <svg viewBox="0 0 10 8.33">
                                <use xlinkHref="./dist/svg/svg-sprite.svg#tw" />
                            </svg>
                        </a>
                    </li>
                    <li className="lex-social__item lex-social__item--facebook">
                        <a href={ this.social[1].url } target="_blank">
                            { this.social[1].name }
                            <svg viewBox="0 0 4.4 9.3">
                                <use xlinkHref="./dist/svg/svg-sprite.svg#fb" />
                            </svg>
                        </a>
                    </li>
                    <li className="lex-social__item lex-social__item--linkedin">
                        <a href={ this.social[2].url } target="_blank">
                            { this.social[2].name }
                            <svg viewBox="0 0 9 8.5">
                                <use xlinkHref="./dist/svg/svg-sprite.svg#in" />
                            </svg>
                        </a>
                    </li>
                    <li id="lex-share-icon" className="lex-social__item lex-social__item--mobileshare">
                        <SocialBtn menuOpen={ this.state.menuOpen } onClick={ this.toggleMobileShareDialogue } />
                    </li>
                </ul>
            </div>
        )
    }
}

class SocialBtn extends React.Component {
    constructor(props) {
        super(props);
    }

    toggleMobileShareDialogue(e){
        e.preventDefault();
        e.stopPropagation();
        e.nativeEvent.stopImmediatePropagation();
        this.props.onClick(this.props.menuOpen, e);
    }

    render(){
        return(
            <a className={this.props.menuOpen ? 'menuOpen': ''} href="javascript:;" onClick={ this.toggleMobileShareDialogue.bind(this) } >
                <svg className="lex-social--share" width="100%" viewBox="0 0 15.74 17.81">
                    <use xlinkHref="./dist/svg/svg-sprite.svg#share-icon" />
                </svg>
                <svg className="lex-social--close" width="100%" viewBox="0 0 12.66 12.66">
                    <use xlinkHref="./dist/svg/svg-sprite.svg#close" />
                </svg>
            </a>
        )
    }
}

ReactDOM.render(<ReactSocial />, document.getElementById('lex-social-js'));
