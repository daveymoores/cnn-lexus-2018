class DesktopShare {
    constructor(node){
        this.node = node;
        this.init();
    }

    init(){

        var _href = window.location.href;
        _href = _href.split('#');
        _href = _href[0];
        this.shareAnchors = this.node.getElementsByTagName('a');

        [].forEach.call(this.shareAnchors, (el, index, array)=>{
            el.setAttribute('href', el.getAttribute('href').replace("SHAREURLHERE", _href));
        });

    }
}

module.exports = DesktopShare;
