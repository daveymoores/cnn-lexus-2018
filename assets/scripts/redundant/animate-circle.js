import Poll from './poll';
import {Promise} from "es6-promise-polyfill";
import ScrollHandler from './scroll-handler.js';
import HashNavigation from './hash-navigation.js';
import ReactSocial from './react-social.js';
import ReactNavigation from './react-navigation.js';
import { ACTIONS, TEXTURES, COLORS } from './variables.js';
import Store from './store';
var store = new Store();

class CircleAnimate {
    constructor(node){
        this.node = node;
        this.init();
    }

    init(){
        this.scrollHandler = new ScrollHandler();
        this.article = new HashNavigation();
        this.bg =  document.getElementById('lex-background');
        this.setUrl();
        var cxt = this;

        var orig = this.node.querySelector('.circle'),
            btn = this.node.querySelector('.circle-btn'),
            length,
            timer;

        function getCircleLength(el){
            var r = el.getAttribute('r');
            var circleLength = 2 * Math.PI * r;
            return circleLength;
        }

        var obj = {length:0,
                   pathLength: getCircleLength(orig)};

        orig.style.stroke = COLORS['pink'];
        drawLine();

        this.t = TweenMax.to(obj, 2, {length:obj.pathLength, onUpdate:drawLine, ease:Linear.easeNone});

        function reverse(){
            cxt.t.reverse();
        }

        this.node.addEventListener('mousedown', this.play.bind(this));
        this.node.addEventListener('mouseup', reverse, true);

        function drawLine() {
            if(obj.length < obj.pathLength) {
                orig.style.strokeDasharray = [obj.length,obj.pathLength].join(' ');
            } else {
                orig.style.strokeDasharray = [obj.pathLength,obj.pathLength].join(' ');
                cxt.node.removeEventListener('mouseup', reverse, true);
                TweenMax.to(orig, 0.6, {scale: 0.2, delay: 0, opacity: 0, transformOrigin:"50% 50%", ease:Power4.easeInOut});
                TweenMax.to(btn, 0.6, {scale: 0, delay: 0.1, transformOrigin:"50% 50%", ease:Power4.easeInOut});

                setTimeout(()=>{
                    cxt.reset(orig, btn);
                }, 2000);
            }
        }

        this.t.pause();
    }

    setUrl(){
        var url = this.article.parseUrl();
        if(url == ""){
            //if no hash set  url to first article
            history.pushState(null, null, `#!article=craftsmanship`);
            url = "craftsmanship";
        }

        this.setBackground(url);
    }

    setBackground(url){
        this.textures = TEXTURES;

        TweenMax.set(this.bg, {opacity: 0})
        this.bg.style.backgroundImage = `url(${this.textures[url]})`;
        TweenMax.to(this.bg, 1.5, {opacity: 0.9, ease:Linear.easeNone});
    }

    reset(circle, btn){
        TweenMax.set(circle, {scale: 1, opacity: 1});
        TweenMax.set(btn, {scale: 1, opacity: 1});
    }

    play(){
        this.t.play();
        var state = store.getState();
        console.log(store.getState());

        return Poll(() => {
            store.dispatch(ACTIONS.animating);
            return (this.t.progress() === 1);
        }, 2500, 5).then(() => {
            setTimeout(()=>{
                this.scrollHandler.loadArticle();
            }, 300);
            return true;
        });
    }
}


module.exports = CircleAnimate;


//MARKUP for animated circle-------------
// <div class="lex-btn--circle--wrapper">
//     <p class="lex-btn--circle--prompt">press and hold to begin</p>
//     <svg class="lex-btn--circle" id="fe2da3f5-ce2d-46b4-8485-d74feb103fca" data-name="Layer 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 300 300">
//       <g>
//         <circle class="circle" cx="150" cy="150" r="120" stroke-width="20" style="fill: none"/>
//         <circle class="circle-btn" cx="105" cy="105" r="36" transform="translate(45 45)" style="fill: #fff"/>
//       </g>
//     </svg>
// </div>
//END MARKUP for animated circle-------------
