((() => {
	'use strict';

	let widgets;
    const $body = document.body;

	widgets = {
		"entry-point" : require("./widgets/entry-point")
	};

    function initWidgets ($node) {
        let dw = $node.querySelectorAll('[data-widget]');
        Array.prototype.forEach.call(dw, (el, index, array) => {
            let type;
            type = el.getAttribute('data-widget');

            if (widgets[type]) {
                new widgets[type](el);
            }
        });
    }

    initWidgets($body);

})());
