var path = require('path');
var webpack = require('webpack');
const UglifyJsPlugin = require('uglifyjs-webpack-plugin');

module.exports = {
   cache: true,
   debug: true,
   devtool: 'eval',
   entry: './assets/scripts/main.js',
   output: {
      path: path.join(__dirname, "dist"),
      filename: 'main.min.js'
   },
   resolve: {
      extensions: ['', '.js', '.json', '.coffee'],
      alias: {
        "TweenLite": path.resolve('node_modules', 'gsap/src/minified/TweenLite.min.js'),
        "TweenMax": path.resolve('node_modules', 'gsap/src/minified/TweenMax.min.js'),
        "TimelineLite": path.resolve('node_modules', 'gsap/src/minified/TimelineLite.min.js'),
        "TimelineMax": path.resolve('node_modules', 'gsap/src/minified/TimelineMax.min.js'),
        "ScrollTo": path.resolve('node_modules', 'gsap/src/minified/plugins/ScrollToPlugin.min.js'),
        "ScrollMagic": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/ScrollMagic.js'),
        "animation.gsap": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/animation.gsap.js'),
        "debug.addIndicators": path.resolve('node_modules', 'scrollmagic/scrollmagic/uncompressed/plugins/debug.addIndicators.js'),
        "react": "preact-compat",
        "react-dom": "preact-compat"
      }
  },
  module: {
    loaders: [{
      test: /\.js$/,
      exclude: /node_modules/,
      loader: 'babel',
      query: {
        presets: ['env', 'react'],
      }
    }]
}
};
