<!doctype html>
<html lang="en">
<head>

	<title>CNN/Lexus - Pioneering Spirits</title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0" />
    <meta charset="UTF-8">

	<meta name="description" content="Pioneering Spirits. These skilful, dedicated individuals are committed to perfection, and the legacy of their craft.">
	<meta name="keywords" content="cars, luxury">

	<meta property="og:locale" content="en_GB" />
	<meta property="og:type" content="website" />
	<meta property="og:title" content="CNN - Pioneering Spirits" />
	<meta property="og:url" content="http://advertisementfeature.cnn.com/2018/lexuspioneeringspirits/" />
	<meta property="og:image" content="./dist/images/social.jpg" />

	<meta property="og:site_name" content="CNN/Lexus - Pioneering Spirits" />
	<meta property="og:description" content="Pioneering Spirits. These skilful, dedicated individuals are committed to perfection, and the legacy of their craft." />
	<meta name="twitter:card" content="summary_large_image"/>
	<meta name="twitter:description" content="Pioneering Spirits. These skilful, dedicated individuals are committed to perfection, and the legacy of their craft."/>
	<meta name="twitter:title" content="CNN - Pioneering Spirits"/>
	<meta name="twitter:image:src" content="./dist/images/social.jpg"/>
	<meta name="twitter:creator" content="CNN Create - Lexus"/>

    <link href="./dist/styles/main.css" rel="stylesheet">

</head>
<body>

<div id="cnn-header">
	<a href="http://cnn.com" class="cnn-back">Back to CNN content</a>
	<a href="https://www.lexus.eu/" target="_blank" class="cnn-sponsor">Sponsored Content by Lexus</a>
	<div class="cnn-menu">
		<a href="#">&copy; 2017 Cable News Network. Turner Broadcasting System Europe, limited. All Rights Reserved.</a>
		<a href="http://edition.cnn.com/terms">Terms of service</a>
		<a href="http://edition.cnn.com/privacy">Privacy guidelines</a>
		<a href="http://edition.cnn.com/#">Ad choices</a>
		<a href="http://edition.cnn.com/about">About us</a>
		<a href="http://edition.cnn.com/feedback">Contact us</a>
		<a href="http://www.turner.com/careers">Work for us</a>
		<a href="http://edition.cnn.com/help">Help</a>
	</div>
	<a href="http://cnn.com" target="_blank" class="cnn-transparent-overlay"></a>
	<script>(function(){if(!String.prototype.trim){String.prototype.trim=function(){return this.replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g,'');};} var add = function(n,c){ return (n.className = (rem(n,c) + ' ' + c).trim()); }, rem = function(n,c){ return (n.className = (' ' + n.className + ' ').replace(' ' + c + ' ', ' ').trim()); }, header = document.getElementById('cnn-header'), menu = header.querySelector('.cnn-menu'), isset = false, issetMenu = false; window.addEventListener('scroll', function(){ var toBeSet = (pageYOffset ? pageYOffset : (document.documentElement.clientHeight ? document.documentElement.scrollTop : document.body.scrollTop)) > header.clientHeight / 2; if( !isset && toBeSet ){ add(header,'fixed'), isset = true; } else if( isset && !toBeSet ){ rem(header,'fixed'), isset = false; } }), menu.addEventListener('click', function( event ){ if( event.target === menu ){ if( !issetMenu ){ menu.className = add(menu,'active'), issetMenu = true; } else { menu.className = rem(menu,'active'), issetMenu = false; }} }); })();</script>
</div>

<div class="lex-wrapper no-article-selected" id="lex-scroll-hook-js" data-widget="entry-point">
	<div id="lex-loader" class="lex-global-ui-components--loader">
		<!-- <svg width="100%" viewBox="0 0 65 65">
			<use xlink:href="./dist/svg/svg-sprite.svg#loader" />
		</svg> -->

		<div class="vp-spin">
			<svg class="vp-spin-trace" viewBox="0 0 50 50">
				<circle cx="50%" cy="50%" r="20"></circle>
			</svg>
			<svg class="vp-spin-circle" viewBox="0 0 50 50">
				<circle cx="50%" cy="50%" r="20"></circle>
			</svg>
		</div>
	</div>
	<div class="lex-global-ui-components">
			<h1 class="lex-global-ui-components--title" id="lex-title">
				<a href="#craftsmanship">Pioneering Spirits</a>
			</h1>
			<div class="lex-arrow" id="lex-arrow-js"></div>
			<div id="lex-navigation-js" class="lex-navigation--wrapper"></div>
			<div class="lex-global-ui-components--logo">
				<a href="https://www.lexus.eu/" target="_blank">
					<img src="./dist/images/lexus-logo.png" alt="" />
				</a>
			</div>
			<div class="lex-global-ui-components--social" id="lex-social-js"></div>
	</div>

	<div class="lex-background--texture" id="lex-canvas-wrap-js">
		<div></div>
	</div>

	<div class="lex-background--texture--mask" id="lex-mask-js"></div>

	<article class="lex-article">

		<div id="lex-injection-js" class="lex-injection--wrapper">

			<?php include "./partials/articles/craftsmanship.php" ?>
			<?php include "./partials/articles/technology.php" ?>
			<?php include "./partials/articles/design.php" ?>
			<?php include "./partials/articles/performance.php" ?>

			<a href="#craftsmanship" class="lex-top-prompt">
				<svg width="100%" viewBox="0 0 16.77 9.15">
					<use xlink:href="./dist/svg/svg-sprite.svg#down-chevron" />
				</svg>
				<span>Back to top</span>
			</a>

		</div>

	</article>
</div>

<!--responsive beacons-->
<div class="visible--only-sm" id="js-sm-beacon"></div>
<div class="visible--only-md" id="js-md-beacon"></div>
<div class="visible--only-lg" id="js-lg-beacon"></div>
<!--end responsive beacons-->

<script type="text/javascript" src="./svg-polyfill/svg4everybody.min.js"></script>
<script src="dist/scripts/main.min.js"></script>
<script>svg4everybody();</script>

<script type="text/javascript">
window.digitalDataLayer = {
	'page_template': 'article',
	'sponsored_content': true,
	'sponsorship_type': 'adfeature',
	'advertiser_name': 'Lexus',
	'ad_enabled': false,
	'campaign_start_date': '12/03/2018',
	'campaign_end_date': '01/09/2018',
	'campaign_geo_targeting': 'us,ca,emea',
	'article_total_pages': 1
}
</script>
<script type="text/javascript" src="http://assets.adobedtm.com/163d1096ee58e69f3c2853388c3bd41996e5fc4f/satelliteLib-d76d1730abb96687bfefdfdb53da02b40150601c.js"></script>
<script type="text/javascript">_satellite.pageBottom();</script>

</body>
</html>
