<article data-art="exh" id="performance">
    <header class="lex-header" data-fx="title-sequence">
        <div class="lex-header--pin-wrapper">
            <div class="lex-header--pin-text-wrapper">
                <!--this is only shown on site load-->
                <div class="lex-header--site-title-wrapper">

                    <h1 class="lex-header--htag"><span>Pioneering</span><br /> <span>spirits</span></h1>
                    <p class="lex-header--ptag">From traditional wood and craft techniques to rethinking the way we hear and power the world, meet four talented innovators who are transforming our world one idea at a time.</p>

                </div>

                <div class="lex-header--article-title-wrapper">

                    <?php include "./partials/characters/performance.php" ?>
                    <h2 class="lex-header--htag"><span>Performance</span></h2>
                    <p class="lex-header--ptag">
                        <span class="lex-header--ptag--progress-line"></span>
                        Pioneering technology is a step in the right direction. Sometimes to find truly great ideas you need to not just think outside the box, but to throw the box away and start again.
                    </p>

                </div>

            </div>
        </div>
    </header>

    <section class="lex-section lex-section--full-width">
        <div class="lex-parallax--img-wrapper">
            <img alt="" class="lex-parallax--img lazy" data-src="./dist/images/article-images/performance/1.jpg" />
        </div>
    </section>

    <section class="lex-section" data-fx="intro">
        <div class="lex-grid--row">
            <div class="lex-grid--column-2">
                <div class="lex-parallax--img--line-wrapper">
                    <span>
                        <img alt="" class="lex-parallax--img desktop lazy" data-src="./dist/images/article-images/performance/2.jpg" />
                        <img alt="" class="lex-parallax--img mobile lazy" data-src="./dist/images/article-images/performance/2-mob.jpg" />
                    </span>
                </div>
            </div>
            <div class="lex-grid--column-2">
                <p class="small drop-cap push-top last-small">Such was the approach of industrial designer and tech innovator Laurence Kemball-Cook, who was determined to find a totally new creative solution to the growing global issue of climate change.</p>

                <p class="small">Eager to find a complementary alternative to wind and solar energy, he struck upon a novel approach during his daily commute to Loughborough University, where he was completing an Industrial Design and Technology degree.</p>

                <p class="small">“I would pass through Victoria Station, which processes 75 million people a year, and began to think seriously about the potential to harvest some of the kinetic energy of people walking,” he says.</p>
            </div>
        </div>

    </section>

    <section class="lex-section" data-fx="content">

        <div class="lex-grid--row">
            <div class="lex-grid--column-2"></div>
            <div class="lex-grid--column-2">
                <div class="lex-parallax--img-wrapper blockquote-bg">
                    <blockquote class="lex-blockquote">Inspired by my own research into renewables and city infrastructure, I had the inspiration to harness energy from city inhabitants, via their footsteps.</blockquote>

                    <div class="lex-parallax--img--line-wrapper">
                        <span>
                            <img alt="" class="lex-parallax--img lazy" data-src="./dist/images/article-images/performance/3.jpg" />
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="lex-grid--row">
            <div class="lex-grid--column-2">
                <p class="small drop-cap">Kemball-Cook launched his company Pavegen in 2009, and began to develop a clever new paving system. It uses the weight of pedestrian footfall to compress a series of electro-magnetic generators, creating a rotary motion that produces small amounts of energy that can be collected and stored in batteries or deployed locally to power applications such as lighting, sensors and data transmission.</p>

                <p class="small">He immediately began looking for opportunities to promote his smart flooring solution, and initially had to resort to some unorthodox methods to attract new clients.</p>

                <p class="small">“To get our first sale, we installed a Pavegen tile on a building site without permission and then used social media to successfully persuade the developer to acquire a Pavegen system,” he admits. “Since then we have attracted an international client base, a network of international partners and a senior team.”</p>
            </div>
            <div class="lex-grid--column-2">

                <p class="small">It didn’t take long for news to spread about the Pavegen system, and to date Kemball-Cook’s team have installed over 200 permanent and experiential projects in 30 countries around the world, including Dupont Circle in Washington DC and various locations at the centre of the city of London.</p>

                <p class="small">“Right from the very first prototypes, our tech has generated great interest, from businesses wanting to work with us to a diverse group of investors and, of course, the people using Pavegen,” he says.</p>
            </div>
        </div>

    </section>

    <section class="lex-section lex-section--full-width">
        <div class="lex-parallax--img-wrapper">
            <img alt="" class="lex-parallax--img lazy" data-src="./dist/images/article-images/performance/4.jpg" />
        </div>
    </section>

    <section class="lex-section--header-placeholder lex-section lex-section--full-width lex-parallax--sticky-vid-parent" data-fx="sticky-heading">
        <div class="lex-parallax--sticky-vid-wrapper">
            <div class="lex-grid--row">
                <div class="lex-grid--column-2">
                    <p class="small drop-cap">With a range of cities and major corporations, such as Google and Nike, lining up to work with Pavegen, the future of kinetic sustainable energy certainly looks bright.</p>

                    <p class="small">Kemball-Cook believes the system has great potential to integrate with other energy sources, and to be used across a range of applications, from transport hubs to schools and retail centres, to create smarter, more efficient cities that actively engage with the issue of climate change.</p>
                </div>
                <div class="lex-grid--column-2">
                    <p class="small">“By solving the energy crisis, we will take away a key challenge for communities, allowing more creativity and energy to be deployed on fixing some of our other problems such as food insecurity and the rapid loss of biodiversity,” he says.</p>
                    <p class="small">“We only have the one planet so it’s vital we protect the natural resources that we all depend on.”</p>
                </div>
            </div>
        </div>
    </section>

    <section class="lex-section lex-section--full-width" data-fx="gallery">
        <div class="lex-gallery--track">
            <div class="lex-gallery--wrapper">
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/performance/5.jpg" data-img="0"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/performance/6.jpg" data-img="1"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/performance/7.jpg" data-img="2"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/performance/8.jpg" data-img="3"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/performance/9.jpg" data-img="4"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/performance/10.jpg" data-img="5"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/performance/11.jpg" data-img="6"></div>
            </div>
        </div>
    </section>

    <section class="lex-section lex-section--car-callout">
        <div class="lex-grid--row">
            <div class="lex-grid--column-3">
                <div class="lex-grid--column-2">
                    <img class="lazy" data-src="./dist/images/article-images/performance/12.jpg" alt="" />
                </div>
                <div class="lex-grid--column-1">
                    <div class="lex-section--car-callout--copy">
                        <heading>
                            <h3>Stepping into the future</h3>
                        </heading>

                        <p class="small">Innovation and originality are at the core of the Lexus approach. Always working at the cutting edge of new technology, and collaborating with the very best scientists and technicians in their fields, it is a true passion for finding ground-breaking solutions that sets Lexus above and beyond the competition.</p>

                        <a href="https://www.lexus.eu/discover-lexus/performance/#hero" target="_blank">more</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
