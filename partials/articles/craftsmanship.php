<article data-art="cra" id="craftsmanship">
    <header class="lex-header" data-fx="title-sequence">
        <div class="lex-header--pin-wrapper">
            <div class="lex-header--pin-text-wrapper">

                <!--this is only shown on site load-->
                <div class="lex-header--site-title-wrapper">

                    <h1 class="lex-header--htag"><span>Pioneering</span><br /> <span>spirits</span></h1>
                    <p class="lex-header--ptag">From traditional wood and craft techniques to rethinking the way we hear and power the world, meet four talented innovators who are transforming our world one idea at a time.</p>

                </div>

                <div class="lex-header--article-title-wrapper">

                    <?php include "./partials/characters/craftsmanship.php" ?>
                    <h2 class="lex-header--htag"><span>Craftsmanship</span></h2>
                    <p class="lex-header--ptag">
                        <span class="lex-header--ptag--progress-line"></span>
                        The art of carrying a tradition from generation to generation. Some art forms are so uniquely exquisite that they don’t just survive throughout the centuries, but actually thrive.
                    </p>

                </div>

            </div>
        </div>
    </header>

    <section class="lex-section lex-section--full-width lex-parallax--sticky-vid-parent" data-fx="sticky-clip">
        <div class="lex-parallax--sticky-vid-wrapper">
            <div data-type="vimeo" class="desktop-video" data-video-id="257897729"></div>
            <div data-type="vimeo" class="mobile-video" data-video-id="258808683"></div>
            <span class="lex-parallax--progress-line"></span>
        </div>
    </section>

    <section class="lex-section" data-fx="intro">
        <div class="lex-grid--row">
            <div class="lex-grid--column-2">
                <div class="lex-parallax--img--line-wrapper">
                    <span>
                        <img alt="" class="lex-parallax--img desktop lazy" data-src="./dist/images/article-images/craftsmanship/2.jpg" />
                        <img alt="" class="lex-parallax--img mobile lazy" data-src="./dist/images/article-images/craftsmanship/2-mob.jpg" />
                    </span>
                </div>
            </div>
            <div class="lex-grid--column-2">
                <p class="small drop-cap push-top last-small">The intricate method of Edo kiriko glass carving is a prime example of this, and the craftspeople still practising this 200-year-old skill are proud to continue such a rich and valued custom. Yet they are still modern makers and careful about restricting their work by describing it as strictly traditional. </p>

                <p class="small">“Tradition really is a big word, laden with lots of different meanings,” explains third generation Edo kiriko artisan Yoshiro Kobayashi. </p>
            </div>
        </div>
    </section>

    <section class="lex-section" data-fx="content">

        <div class="lex-grid--row">
            <div class="lex-grid--column-2"></div>
            <div class="lex-grid--column-2">
                <div class="lex-parallax--img-wrapper blockquote-bg">
                    <blockquote class="lex-blockquote">It’s not something that is forced on anybody nor is it something that people carry on their backs, [but] something which I believe is born out of daily human activity and created organically from the repetition of that custom... something that connects everything.</blockquote>

                    <div class="lex-parallax--img--line-wrapper">
                        <span>
                            <img alt="" class="lex-parallax--img lazy" data-src="./dist/images/article-images/craftsmanship/3.jpg" />
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="lex-grid--row">
            <div class="lex-grid--column-2">
                <p class="small drop-cap">Precise repetition is crucial to the art of Edo kiriko and years of practise and training are essential for any aspiring craftsperson. Kobayashi, like many of his peers, began his education at the tender age of 13 when he joined his father in his workshop.</p>

                <p class="small">“[Back then] you would shadow the senior artisans and learn from observing... you’d be up for 10 hours a day practising,” he says. “It really was gruelling work.”</p>

                <p class="small">He worked hard at developing his skills and admits he didn’t really feel like a true professional until he was recognised by his peers with a special award at the age of 33.</p>

            </div>
            <div class="lex-grid--column-2">
                <p class="small">It’s a demanding apprenticeship by any measure, but the years of training are essential as this form of glasswork, which originated in the Tokyo area at the end of the Edo period, is extremely meticulous and requires a degree of perfectionism that echoes the detail-oriented nature of Japanese culture.</p>

                <p class="small">“The Japanese pay attention to small intricacies,” says Kobayashi. “They really do care about the details... it is almost subconscious. Something that becomes a way of life without you realising it.”</p>
            </div>
        </div>

    </section>

    <section class="lex-section lex-section--full-width">
        <div class="lex-parallax--img-wrapper">
            <img alt="" class="lex-parallax--img lazy" data-src="./dist/images/article-images/craftsmanship/4.jpg" />
        </div>
    </section>

    <section class="lex-section--header-placeholder lex-section lex-section--full-width lex-parallax--sticky-vid-parent" data-fx="sticky-heading">
        <div class="lex-parallax--sticky-vid-wrapper">
            <div class="lex-grid--row">
                <div class="lex-grid--column-2">
                    <p class="small drop-cap">For Kobayashi, the complexity of his work is crucial to ensuring a high quality finished product, as well as being central to the deep sense of fulfilment and pleasure that he derives from his craft.</p>

                    <p class="small">“I’m certain this philosophy especially impacts an artisan’s work,” he says. “The tiniest intricacy takes on great importance. This is why they take great care whenever making anything. It’s about the personal satisfaction that the artisan derives from his work.”</p>

                    <p class="small">Although he is passionate about the craft of Edo kiriko and has derived enormous pleasure over the years, Kobayashi did not pressure his son to follow in his footsteps but rather encouraged him to explore alternative career options. </p>
                </div>
                <div class="lex-grid--column-2">
                    <p class="small">However the call of family history and tradition proved too strong, and his son Yohei has joined him in the family business, bringing a contemporary eye to the pieces but also continuing to produce work that has been appreciated and perfected by previous generations.</p>

                    <p class="small">“Being able to do something you enjoy and make a living from it is wonderful,” Kobayashi says. “I feel glad that I chose this work.”</p>
                </div>
            </div>
        </div>
    </section>

    <section class="lex-section lex-section--full-width" data-fx="gallery">
        <div class="lex-gallery--track">
            <div class="lex-gallery--wrapper">
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/craftsmanship/5.jpg" data-img="0"></div>
                <div class="lex-gallery--item lazy" style="background-image: url(./dist/images/article-images/craftsmanship/6.jpg);" data-img="1"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/craftsmanship/7.jpg" data-img="2"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/craftsmanship/8.jpg" data-img="3"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/craftsmanship/9.jpg" data-img="4"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/craftsmanship/10.jpg" data-img="5"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/craftsmanship/11.jpg" data-img="6"></div>
            </div>
        </div>
    </section>

    <section class="lex-section lex-section--car-callout">
        <div class="lex-grid--row">
            <div class="lex-grid--column-3">
                <div class="lex-grid--column-2">
                    <img class="lazy" data-src="./dist/images/article-images/craftsmanship/12.jpg" alt="" />
                </div>
                <div class="lex-grid--column-1">
                    <div class="lex-section--car-callout--copy">
                        <heading>
                            <h3>Generations of craft</h3>
                        </heading>

                        <p class="small">Edo Kiriko glassmakers made the exquisite panel inside the Lexus LS, which greets passengers with an ambience that’s at once respectfully gentle, and breathtakingly impressive.</p>

                        <a href="https://www.lexus.eu/discover-lexus/craftsmanship/#hero" target="_blank">more</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
