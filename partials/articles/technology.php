<article data-art="ima" id="technology">
    <header class="lex-header" data-fx="title-sequence">
        <div class="lex-header--pin-wrapper">
            <div class="lex-header--pin-text-wrapper">
                <!--this is only shown on site load-->
                <div class="lex-header--site-title-wrapper">

                    <h1 class="lex-header--htag"><span>Pioneering</span><br /> <span>spirits</span></h1>
                    <p class="lex-header--ptag">From traditional wood and craft techniques to rethinking the way we hear and power the world, meet four talented innovators who are transforming our world one idea at a time.</p>

                </div>

                <div class="lex-header--article-title-wrapper">

                    <?php include "./partials/characters/technology.php" ?>
                    <h2 class="lex-header--htag"><span>technology</span></h2>
                    <p class="lex-header--ptag">
                        <span class="lex-header--ptag--progress-line"></span>
                        Sound, with all its complexity and beauty, is a vital part of our everyday lives – and for those who are tuned into the value of well-designed aural events, there is nothing more exciting than truly superior sound.
                    </p>

                </div>

            </div>
        </div>
    </header>

    <section class="lex-section lex-section--full-width">
        <div class="lex-parallax--img-wrapper">
            <img alt="" class="lex-parallax--img lazy" data-src="./dist/images/article-images/technology/4.jpg" />
        </div>
    </section>

    <section class="lex-section" data-fx="intro">
        <div class="lex-grid--row">
            <div class="lex-grid--column-2">
                <div class="lex-parallax--img--line-wrapper">
                    <span>
                        <img alt="" class="lex-parallax--img desktop lazy" data-src="./dist/images/article-images/technology/2.jpg" />
                        <img alt="" class="lex-parallax--img mobile lazy" data-src="./dist/images/article-images/technology/2-mob.jpg" />
                    </span>
                </div>
            </div>
            <div class="lex-grid--column-2">
                <p class="small drop-cap push-top last-small">One of these people is Dutch composer and technologist Paul Oomen, who says that sound has always been his “way to imagine”.</p>

                <p class="small">“[As a child] I would be humming melodies that came up spontaneously, and that would evoke a world of sounds in my imagination beyond the melodies themselves,” he explains.</p>

                <p>These early childhood explorations into what he could hear were the promising beginning of a lifelong passion, and Oomen has devoted his career to exploring the intricately layered relationship that exists between humans and the sound world they encounter.</p>
            </div>
        </div>
    </section>

    <section class="lex-section" data-fx="content">

        <div class="lex-grid--row">
            <div class="lex-grid--column-2"></div>
            <div class="lex-grid--column-2">
                <div class="lex-parallax--img-wrapper blockquote-bg">
                    <blockquote class="lex-blockquote">“I am listening to what happens in between sounds and the space, and in between sounds and my body,”</blockquote>

                    <div class="lex-parallax--img--line-wrapper">
                        <span>
                            <img alt="" class="lex-parallax--img lazy" data-src="./dist/images/article-images/technology/3.jpg" />
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="lex-grid--row">
            <div class="lex-grid--column-2">
                <p class="small drop-cap">Oomen, who lives and works in Budapest where he founded the Spatial Sound Institute in 2015, originally focused on creating sound designs composing music for theatre and opera. But in 2007 he shifted his attention to the exploration of space, sound and perception with his company 4DSOUND, where he worked with his team to create more than 70 close to a hundred spatial sound projects to date, and developed a strong interest in enhancing the experience of listening.</p>
            </div>
            <div class="lex-grid--column-2">
                <p class="small">“The more we refine our listening experience, the more we listen <em>spatially</em>,” he explains. “A high-quality experience of listening means a sound system that draws no attention to itself, but draws the attention to the environment, hearing sounds as a part of and moving in the environment. The sound system is only one element, the design of the space, its interior treatment, the social aspects and – not in the least – the sound content are all elements that need to match to transform the experience as a whole.”</p>
            </div>
        </div>

    </section>

    <section class="lex-section lex-section--full-width">
        <div class="lex-parallax--img-wrapper">
            <img alt="" class="lex-parallax--img" src="./dist/images/article-images/technology/1.jpg" />
        </div>
    </section>

    <section class="lex-section--header-placeholder lex-section lex-section--full-width lex-parallax--sticky-vid-parent" data-fx="sticky-heading">
        <div class="lex-parallax--sticky-vid-wrapper">
            <div class="lex-grid--row">
                <div class="lex-grid--column-2">
                    <p class="small drop-cap">For Oomen, this experience relies as much on the active participation of the listener as it does on the quality of the equipment.</p>

                    <p class="small">“Our attention spans have become so short that it takes a big effort for many people to even find the space of mind to focus on something that actually requires ‘listening’,” he says. “If we want to have more refined and experientially engaging listening experiences, we should begin with omitting the noise from our environments, otherwise we simply won’t hear anything.”</p>
                </div>
                <div class="lex-grid--column-2">
                    <p class="small">Although he is still passionate about music, which he has been composing since his youth, and he is surrounded by many layers of multi-dimensional sound every day in his work and his private life, Oomen admits his favourite thing to listen to is peace and quiet.</p>

                    <p class="small">“In my daily life, the thing I long to hear mostly is silence,” he admits. “Silence is relative of course, so instead of silence I should probably say: less noise and more space, so my senses can open up and breathe, and I start hearing more.”</p>
                </div>
            </div>
        </div>
    </section>

    <section class="lex-section lex-section--full-width" data-fx="gallery">
        <div class="lex-gallery--track">
            <div class="lex-gallery--wrapper">
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/technology/5.jpg" data-img="0"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/technology/6.jpg" data-img="1"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/technology/7.jpg" data-img="2"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/technology/8.jpg" data-img="3"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/technology/9.jpg" data-img="4"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/technology/10.jpg" data-img="5"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/technology/11.jpg" data-img="6"></div>
            </div>
        </div>
    </section>

    <section class="lex-section lex-section--car-callout">
        <div class="lex-grid--row">
            <div class="lex-grid--column-3">
                <div class="lex-grid--column-2">
                    <img class="lazy" data-src="./dist/images/article-images/technology/12.jpg" alt="" />
                </div>
                <div class="lex-grid--column-1">
                    <div class="lex-section--car-callout--copy">
                        <heading>
                            <h3>THE SOUND OF SUCCESS:</h3>
                        </heading>

                        <p class="small">A superior auditory experience can be game changing. From the location of the speakers to the quality of the sound, all aspects of the listening solutions in Lexus cars have been carefully designed to ensure a more refined and engaging sound experience every single time.</p>

                        <a href="https://www.lexus.eu/discover-lexus/technology/#hero" target="_blank">more</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
