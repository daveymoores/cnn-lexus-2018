<article data-art="ima" id="design">
    <header class="lex-header" data-fx="title-sequence">
        <div class="lex-header--pin-wrapper">
            <div class="lex-header--pin-text-wrapper">
                <!--this is only shown on site load-->
                <div class="lex-header--site-title-wrapper">

                    <h1 class="lex-header--htag"><span>Pioneering</span><br /> <span>spirits</span></h1>
                    <p class="lex-header--ptag">From traditional wood and craft techniques to rethinking the way we hear and power the world, meet four talented innovators who are transforming our world one idea at a time.</p>

                </div>

                <div class="lex-header--article-title-wrapper">

                    <?php include "./partials/characters/design.php" ?>
                    <h2 class="lex-header--htag"><span>Design</span></h2>
                    <p class="lex-header--ptag">
                        <span class="lex-header--ptag--progress-line"></span>
                        Humans have crafted wood since the dawn of time, often producing pieces as primitive as they are interesting – but the intricate Japanese art of Yosegi is a notable exception to the rule.
                    </p>

                </div>

            </div>
        </div>
    </header>

    <section class="lex-section lex-section--full-width">
        <div class="lex-parallax--img-wrapper">
            <img alt="" class="lex-parallax--img lazy" data-src="./dist/images/article-images/design/1.jpg" />
        </div>
    </section>

    <section class="lex-section" data-fx="intro">
        <div class="lex-grid--row">
            <div class="lex-grid--column-2">
                <div class="lex-parallax--img--line-wrapper lazy">
                    <span>
                        <img alt="" class="lex-parallax--img desktop lazy" data-src="./dist/images/article-images/design/2.jpg" />
                        <img alt="" class="lex-parallax--img mobile lazy" data-src="./dist/images/article-images/design/2-mob.jpg" />
                    </span>
                </div>
            </div>
            <div class="lex-grid--column-2">
                <p class="small drop-cap push-top last-small">This delicate and labour intensive form of decorative woodcraft, which has been practised for more than 400 years and is also known as Japanese marquetry, involves the arrangement of wood pieces of different colours and textures into complex mosaic patterns with pinpoint accuracy.</p>

                <p>It’s an extraordinarily complicated process requiring a high degree of skill and experience, which means the popularity of this unique art form has waned somewhat over the past century.</p>
            </div>
        </div>
    </section>

    <section class="lex-section" data-fx="content">

        <div class="lex-grid--row">
            <div class="lex-grid--column-2"></div>
            <div class="lex-grid--column-2">
                <div class="lex-parallax--img-wrapper blockquote-bg">
                    <blockquote class="lex-blockquote">I want to continue and protect the tradition. To do so, I have to change and stay innovative. It is not a matter of ‘to copy or not to be copied’, but to pursue my own originality.</blockquote>

                    <div class="lex-parallax--img--line-wrapper">
                        <span>
                            <img alt="" class="lex-parallax--img lazy" data-src="./dist/images/article-images/design/3.jpg" />
                        </span>
                    </div>
                </div>
            </div>
        </div>

        <div class="lex-grid--row">
            <div class="lex-grid--column-2">
                <p class="small drop-cap">Yet one man is determined to not just continue to practise the ancient art of Yosegi but to give it a contemporary flair and promote its beauty to the world. Ken Ota is widely recognised to be a modern master of Yosegi woodmaking and he is a passionate ambassador of his craft, as committed to respecting the traditional techniques perfected over the centuries as he is eager to embrace innovation in the form.</p>

                <p class="small">“I want to continue and protect the tradition,” explains Ota, who works in an atelier in Hakone, Japan, the birthplace of Yosegi. “To do so, I have to change and stay innovative. It is not a matter of ‘to copy or not to be copied’, but to pursue my own originality.”</p>
            </div>
            <div class="lex-grid--column-2">
                <p>Yosegi pieces are so unique and complex that they cannot be mass produced, and the wood chosen for each handmade item intimately reflects the style and taste of the craftsperson, almost like a ‘signature’.</p>
            </div>
        </div>

    </section>

    <section class="lex-section lex-section--full-width">
        <div class="lex-parallax--img-wrapper">
            <img alt="" class="lex-parallax--img lazy" data-src="./dist/images/article-images/design/4.jpg" />
        </div>
    </section>

    <section class="lex-section--header-placeholder lex-section lex-section--full-width lex-parallax--sticky-vid-parent" data-fx="sticky-heading">
        <div class="lex-parallax--sticky-vid-wrapper">
            <div class="lex-grid--row">
                <div class="lex-grid--column-2">
                    <p class="small drop-cap">Ota, who trained for many years to perfect his craft first at a polytechnic school in Saitama then at the renowned Atelier Kiro in Odawara, creates his pieces from a selection of 25 different kinds of wood, ranging from Hokkaido to Okinawa, which he personally sources from across Japan.</p>

                    <p class="small">For him, the wood he uses to make his highly sought after pieces (including boxes, trays and coasters) is more than just a base material and he makes a point of appreciating and valuing each and every piece.</p>

                    <p class="small">“I want to respect the life taken from the trees cut in the wood,” he says. “It goes the same to other subjects, but I want to stay grateful of what I take. Each wood has its own unique shape and blend, giving the freedom and possibility of expressions, which ultimately leads to its attractiveness.”</p>
                </div>
                <div class="lex-grid--column-2">
                    <p class="small">One of the most distinctive features of Yosegi is that it is extremely time consuming and requires extraordinary patience on the part of the craftsman, which is at odds with the fast paced digital world in which we live. Yet Ota is comfortable with the slow pace that his work requires and embraces it as an opportunity to explore the depths of his work as an artist.</p>

                    <p class="small">“Time does not matter,” he says. “I can only express through what I have inside of me.”</p>
                </div>
            </div>
        </div>
    </section>

    <section class="lex-section lex-section--full-width" data-fx="gallery">
        <div class="lex-gallery--track">
            <div class="lex-gallery--wrapper">
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/design/5.jpg" data-img="0"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/design/6.jpg" data-img="1"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/design/7.jpg" data-img="2"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/design/8.jpg" data-img="3"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/design/9.jpg" data-img="4"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/design/10.jpg" data-img="5"></div>
                <div class="lex-gallery--item lazy" data-src="./dist/images/article-images/design/11.jpg" data-img="6"></div>
            </div>
        </div>
    </section>


    <section class="lex-section lex-section--car-callout">
        <div class="lex-grid--row">
            <div class="lex-grid--column-3">
                <div class="lex-grid--column-2">
                    <img class="lazy" data-src="./dist/images/article-images/design/12.jpg" alt="" />
                </div>
                <div class="lex-grid--column-1">
                    <div class="lex-section--car-callout--copy">
                        <heading>
                            <h3>CARVING A NEW ART FORM FROM AN ANCIENT TRADITION:</h3>
                        </heading>

                        <p class="small">Lexus is passionate about innovation and high quality craftsmanship. By celebrating this extraordinary technique of transforming natural wood into elaborate new patterns, Lexus is reinforcing its ongoing commitment to the delicate fusion of tradition and technology.</p>

                        <a href="https://www.lexus.eu/discover-lexus/design/" target="_blank">more</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
</article>
