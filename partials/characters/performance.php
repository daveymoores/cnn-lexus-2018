<svg xmlns="http://www.w3.org/2000/svg" class="lex-header--character lex-header--character--wide" viewBox="0 0 176.01 397.56">
    <g id="321ac506-e1b9-441a-8fbb-81bf9e7d8eb3" data-name="Layer 2">
      <g id="9a17ce06-b4a7-4612-9145-4c0b12b531f3" data-name="Layer 1">
        <path class="target" d="M50.53,5.23S38,49.47,2.45,84.31" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
        <path class="target" d="M111.85,0V161.83" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
        <path class="target" d="M50.53,64h124" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
        <path class="target" d="M58.89,161.83H164.81" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
        <path class="target" d="M31,174.9V47.46" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
        <path class="target" d="M89.55,227.53V367.22c0,26.84-1.71,26.84-36.55,26.84" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
        <path class="target" d="M7.62,244.88H171.48" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
        <path class="target" d="M1.31,343.08H176" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
        <path class="target" d="M18.78,319H147.39v49.4H18.78" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
        <path class="target" d="M147.39,294.28H31v-25.6H147.39Z" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
      </g>
    </g>
</svg>
