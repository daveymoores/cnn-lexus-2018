<svg xmlns="http://www.w3.org/2000/svg" class="lex-header--character lex-header--character--wide" viewBox="0 0 174.82 400.79">
  <g data-name="Layer 2">
    <g data-name="Layer 1">
      <g>
          <path class="target" d="M32.2,0V136.14c0,25.06,0,25-27.28,25" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M1.82,41.78H59.2" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M60.33,80.16C32.48,102.1,1.82,105.48,1.82,105.48" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M117.69,0V77.88" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M64.54,33.83H170.85" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M69.51,77.88h89.54S149,141.27,66,168" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M170.84,165.61C94.22,137.7,84.7,77.88,84.7,77.88" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M42.65,230.81S32.06,261.23,1.82,279.7" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M124.23,246.16h46.61" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M121.1,295.05h53.72" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M154.36,295.05v79.59c0,10.8-.29,17.06-14.21,17.06H129.48" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M84.7,226.83v174" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M50.89,274.87h67.94" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M103.48,235.64s8.24,15.64,10.8,24.45" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M60.33,293.91S64.82,363.84,46.91,384" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M130.77,370.38c-21.89,0-21.89,0-21.89-17.63V293.91" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M29.29,399.66V305" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
          <path class="target" d="M42.65,278S37,306.14,2.85,332.15" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 7px"/>
      </g>
    </g>
  </g>
</svg>
