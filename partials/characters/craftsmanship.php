<svg xmlns="http://www.w3.org/2000/svg" class="lex-header--character lex-header--character--wide" viewBox="-2 0 175 173.5">
    <g data-name="Layer 2">
        <g data-name="Layer 1">
          <g>
            <path class="target" d="M173 173 0.5 173 0.5 173 0.5 0.5 1 0.5 173 0.5" style="fill: none;stroke-width:6;stroke-dashoffset: 0;stroke-dasharray: 0, 0;"/>
            <g>
              <path class="target" d="M30.25,144.2c12-16.06,18.83-37.75,20.4-65.22.8-14,.22-28.26.22-42.32,32.95-.63,63.07-5.46,90.36-14.28" style="fill:none;stroke-width:6;stroke-dashoffset: 0;stroke-dasharray: 0, 0;"/>
              <path class="target" d="M51 79 102.5 79 157 79 102.92 79 102.92 155.54" style="fill: none;stroke-width:6;stroke-dashoffset: 0;stroke-dasharray: 0, 0;"/>
            </g>
          </g>
        </g>
    </g>
</svg>
