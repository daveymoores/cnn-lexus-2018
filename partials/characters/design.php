<svg xmlns="http://www.w3.org/2000/svg" class="lex-header--character lex-header--character--long" viewBox="0 0 125.26 520.02">
  <g data-name="Layer 2">
    <g data-name="Layer 1">
      <g>
        <path  class="target" d="M106.61,442.26c-8,42.2-47.53,74.26-95.14,74.26" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
        <path  class="target" d="M7.89,419.74A101.66,101.66,0,0,1,46,445.8" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
        <path  class="target" d="M74.23,151.13v51.74a71.43,71.43,0,0,1-38.44,63.39" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
        <path  class="target" d="M26.23,225.13v-74" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
        <path  class="target" d="M.73,183.63h102" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
        <path  class="target" d="M93.84,153.71A54.47,54.47,0,0,1,110,172.46" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
        <path  class="target" d="M106.84,142.84a54.57,54.57,0,0,1,16.21,18.75" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
        <path  class="target" d="M16.73,28.63h68" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
        <path  class="target" d="M4.73,53.63h96" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
        <path  class="target" d="M52.23,54.13V64.71A71.45,71.45,0,0,1,13.79,128.1" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
        <path  class="target" d="M90.13,12.89a54.59,54.59,0,0,1,16.21,18.75" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
        <path  class="target" d="M103.14,2a54.65,54.65,0,0,1,16.2,18.75" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
        <path  class="target" d="M102.21,270.65A176.53,176.53,0,0,1,.73,334" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
        <path  class="target" d="M64.23,309.13v73" style="fill: none;stroke: #8d0c37;stroke-miterlimit: 10;stroke-width: 6px"/>
      </g>
    </g>
  </g>
</svg>
